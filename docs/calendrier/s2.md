# Calendrier du semestre 2

*Remarque : Le calendrier suivant est prévisionnel, il est susceptible d'évoluer.*

* Cours : 4h toutes les semaines, en classe entière
* TD : 1h toutes les semaines, en demi-groupe
* TP : 2h toutes les 2 semaines, en tiers de classe
* Colles : 2h de TP toutes les semaines, en demi-groupe (dont 2 colles évaluées)

| Semaine | Cours | TD | TP | Colles | Devoirs |
| :---: | :---: | :---: | :---: | :---: | :---: |
| 6 (03/02) | 8. [Représentation des nombres](../cours/8_representation_nombres.md) | 10. [Chemin de poids maximal](../td/chemin_maximal.md) | 19. [Diviser pour régner](../tp/ocaml/points_proches.md) | 18. [Algorithmes de tris efficaces](../tp/tris_efficaces.md) | - 15. IE (cours 6, TD 9)<br />- 6. DM (structures séquentielles) |
| Vacances | - | - | - | - | - |
| 9 (24/02) | - 8. [Représentation des nombres](../cours/8_representation_nombres.md)<br />- 9. [Arbres](../cours/9_arbres.md) | 11. [Représentation des nombres](../td/representation_nombres.md) | 19. [Diviser pour régner](../tp/ocaml/points_proches.md) | 20. [Programmation dynamique](../tp/ocaml/compter_briques.md) | - 16. IE (TP 18)<br />- 7. DM (tris efficaces) |
| 10 (03/03) | 9. [Arbres](../cours/9_arbres.md) | 12. [Sélection d'activité](../td/selection_activite.md) | 22. [Arbres binaires en C](../tp/c/arbres_binaires.md) | 21. [Problèmes d'optimisation](../tp/c/problemes_optimisation.md) | 17. IE (représentation des nombres) |
| 11 (10/03) | - 9. [Arbres](../cours/9_arbres.md)<br />- 10. [Exploration exhaustive](../cours/10_exploration_exhaustive.md) | 13. [Karatsuba](../td/karatsuba.md) | 22. [Arbres binaires en C](../tp/c/arbres_binaires.md) | - | 4. DS (cours 5 à 8, TP jusqu'au 21, TD jusqu'au 13) |
| 12 (17/03) | 11. [Graphes](../cours/11_graphes) | 14. [Arbres](../td/arbres.md) | 23. Arbres en OCaml | Colle n° 2 : [programme de révision](../evaluations/colles/2_programme.md) et [planning de passage](../evaluations/colles/2_planning.md) | 8. DM (représentation des nombres) |
| 13 (24/03) | 11. [Graphes](../cours/11_graphes) | 14. [Arbres](../td/arbres.md) | 23. Arbres en OCaml | Colle n° 2 : [programme de révision](../evaluations/colles/2_programme.md) et [planning de passage](../evaluations/colles/2_planning.md) | 18. IE (arbres) |
| 14 (31/03) | 11. [Graphes](../cours/11_graphes) | 15. Raisonner sur les graphes | 24. Sudoku | Colle n° 2 : [programme de révision](../evaluations/colles/2_programme.md) et [planning de passage](../evaluations/colles/2_planning.md) | 5. DS (cours 7 à 9, TP jusqu'au 23, TD jusqu'au 15) |
| Vacances | - | - | - | - | - |
| 17 (21/04) | 12. Ensembles ordonnés et inductifs | 16. Parcours de graphes | 24. Sudoku | 25. Graphes | 9. DM (bases de données) |
| 18 (28/04) | 13. Logique | - | 26. Bases de données | 25. Graphes | 19. IE (graphes) |
| 19 (05/05) | 13. Logique | - | 26. Bases de données | 25. Graphes | 6. DS (cours 9 à 12, TP jusqu'au 25, TD jusqu'au 17) |
| 20 (12/05) | 13. Logique |                         17. Syntaxe et sémantique des formules logiques                         | 27. Logique propositionnelle | Colle 3 |  |
| 21 (19/05) | 14. Bases de données | 18. Problème SAT | 27. Logique propositionnelle | Colle 3 |  |
| 22 (26/05) | 15. Structures à l'aide d'arbres | - | 28. ABR et ARN | Colle 3 | 20. IE (logique) |
| 23 (02/06) | 15. Structures à l'aide d'arbres | 19. Bases de données | 28. ABR et ARN | Colle 3 | 7. DS (cours 11 à 15, TP jusqu'au 28, TD jusqu'au 19) |
| 24 (09/06) | 16. Plus court chemin dans un graphe pondéré | 20. Arbres binaires de recherche et arbres bicolores | 30. Tas | 29. Algorithmique du texte | - |
| 25 (16/06) | 17. Algorithmique du texte | 21. Plus court chemin dans un graphe | 30. Tas | 29. Algorithmique du texte | - |
| 26 (23/06) | - | - | - | - | - |
| Vacances | - | - | - | - | [Récupérez une copie des dépôts avant le 06/07](../recuperer_depot.md) |

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Source des images : *production personnelle*
