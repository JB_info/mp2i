# TD : Bases de données

*Remarque : pour ceux qui souhaitent vérifier leurs requêtes, une base de données [`pizzas.db`](./code/pizzas.db) est disponible sur le framagit.*

On s'intéresse à une base de données concernant la livraison de pizzas, dont voici le schéma relationnel :

* **Pizzas** ($\underline{\text{num}}$ : `INTEGER`, nom : `TEXT`, prix : `REAL`)
* **Clients** ($\underline{\text{id}}$ : `INTEGER`, nom : `TEXT`, prenom : `TEXT`, adresse : `TEXT`, ville : `TEXT`)
* **Commandes** ($\underline{\text{num}}$ : `INTEGER`, id_client $\rightarrow$ Clients.id : `INTEGER`, num_pizza $\rightarrow$ Pizzas.num : `INTEGER`, date : `TEXT`, livre : `TEXT`, paye : `INTEGER`)

La pizza d’une commande peut avoir été livrée ou non (l’attribut « livre » indique l'heure de livraison ou `NULL` si non encore livrée) et le client peut l’avoir payé ou non (l’attribut « paye » vaut 1 si oui et 0 sinon). La date est au format `AAAA-MM-JJ`, et l'heure au format `HHhMM`.

1. Quels sont les attributs de la relation `Pizzas` ? Quels sont les domaines de ces attributs ?
2. Quelles sont les clés primaires de chaque relation ? Y a-t-il des clés étrangères ?
3. Peut-on avoir 2 pizzas avec le même numéro de pizza et le même prix mais avec un nom différent ?
4. Un même client peut-il commander plusieurs fois la même pizza un même jour ?
5. Dans le modèle entité-association dont ce schéma relationnel est issu, nous avions deux types d'entité, "Pizzas" et "Clients", et un type d'association "passer une commande". Dessinez ce modèle entité-association, avec cardinalités.

Pour chaque question suivante, donnez une requête SQL permettant d'y répondre. Si rien n'est précisé, c'est qu'on attend la sélection de tous les attributs. Pensez à renommer les colonnes du résultat de manière appropriée si nécessaire.

6. Quel(s) est(sont) le(s) prix des pizzas de nom Margarita ?
7. Quels sont, sans doublons, les prénoms de clients qui ne sont pas aussi des noms de famille ?
8. Quels sont les prénoms et noms des clients qui n’ont pas payé une pizza qui leur a pourtant été livrée ?
9. Donnez, sans doublons, les prénoms des clients qui ont commandé une pizza à strictement plus de 17€.
10. Quel est le nom de la pizza la plus chère ? S’il y a plusieurs pizzas au plus haut prix, la requête devra donner le nom de toutes ces pizzas.
11. Donnez les noms complets (prénom et nom concaténés) des clients, rangés du client ayant le nom complet le plus long à celui ayant le plus court.
12. Pour chaque client (même ceux qui n'ont jamais rien commandé), donnez son prénom et nom, ainsi que le nombre de pizzas commandées par ce client.
13. Quelles sont, sans doublons, les villes dans lesquelles une Regina et une Provence ont été livrées ?
14. Sachant que les trois premières commandes de la base n'étaient que des tests (et donc pas des vraies commandes), donnez toutes les informations des cinq premières commandes ayant été passées.
15. Quel est le prix de vente moyen des pizzas commandées ? On ne prendra en compte que les pizzas qui ont effectivement été payées.
16. Pour chaque client ayant commandé au moins deux pizzas, donnez son prénom et nom, ainsi que le nombre de pizzas commandées par ce client.
17. Donnez le prénom et le nom des clients dont le prénom commence par un « L » et n'ayant jamais commandé de pizza de plus de 15€.
18. Donnez, dans l'ordre alphabétique, les noms de clients qui ont un nom de pizza.
19. Donnez les noms et prénoms des deux clients qui doivent le plus d'argent à la pizzeria.
20. Donnez toutes les informations des clients qui ont commandé au moins une fois chaque pizza.
21. Quels sont les clients qui ont commandé une Margarita ou une Orientale, mais pas les deux ?
22. Quels sont les noms de pizzas qui contiennent au moins deux caractères ’a’ (minuscule ou majuscule), mais qui ne finissent pas par ’a’ ?
23. En moyenne, combien les clients dépensent-ils au maximum pour une pizza ?
24. Donnez, pour chaque pizza, le nombre de fois où elle a été commandée mais pas payée depuis mai 2015.
25. Donnez le prix moyen des pizzas n'ayant jamais été commandées.
26. Quels sont les sixième, septième et huitième clients ayant reçu le plus de pizzas ?
27. Où habite le client qui a commandé le plus de pizzas ?
28. Combien de clients ont commandé une des deux pizzas les moins chères ?

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BYNCSA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)
