# TD : Plus court chemin dans un graphe

1. Rappelez l'algorithme (en pseudo-code) du parcours permettant d'explorer les sommets d'un graphe non pondéré par distance croissante par rapport au sommet de départ. On renverra un tableau `distances` tel que `distances[i]` donne la distance du sommet de départ au sommet `i`, ainsi qu'un tableau `predecesseurs` tel que  `predecesseurs[i]` indique le prédécesseur de `i` dans le plus court chemin allant du sommet de départ à `i`.

2. Proposer un algorithme qui prend en paramètre un tableau `predecesseurs` tel que décrit ci-dessus, un sommet de départ et un d'arrivée, et renvoie la liste des sommets du plus court chemin allant du départ à l'arrivée.

3. Dessinez le graphe pondéré dont la matrice est ![](img/matrice_pondere.png){width=12%}

4. Voici la succession de matrices obtenues lors de l'application de Floyd-Warshall sur le graphe précédent : ![](img/floyd_warshall.png){width=50%}

    Expliquez d'où viennent les valeurs mises à jour (celles en gras), et vérifiez à l'aide de votre dessin du graphe que les résultats finaux sont cohérents. Calculez les matrices des prédécesseurs correspondantes.

5. Appliquez l'algorithme de Floyd-Warshall au graphe suivant : ![](img/floyd_2.png){width=19%}

6. Donnez la liste d'adjacence du graphe pondéré suivant : ![](img/graphe_pondere.png){width=26%}

7. On applique l'algorithme de Dijkstra au graphe précédent avec M comme sommet de départ. Expliquez les valeurs obtenues, donnez la dernière ligne du tableau, et déduisez-en le plus court chemin de M à S. ![](img/dijkstra.png){width=30%}

8. Appliquez l'algorithme de Dijkstra au graphe suivant, en prenant 0 comme départ : ![](img/dijkstra_2.png){width=15%}

9. Appliquez au graphe suivant l'algorithme de Dijkstra depuis le sommet $a$, puis l'algorithme de Floyd-Warshall : ![](img/dijkstra_fw.png){width=30%}

10. Change-t-on les plus courts chemins si on additionne les poids de toutes les arêtes d’un graphe par une constante? et si on les multiplie par une constante ?

11. Comment pourrait-on déterminer le poids minimum d’un cycle dans un graphe orienté ?

Vous devez savoir implémenter ces deux algorithmes aussi bien en C qu'en OCaml.

12. Implémentez l'algorithme de Floyd-Warshall.
13. Implémentez efficacement une file de priorité, et implémentez alors l'algorithme de Dijkstra.
14. Modifiez vos 2 algorithmes pour renvoyer également les prédécesseurs des sommets dans les plus courts chemins.
15. Implémentez votre algorithme (question 2) de reconstruction d'un plus court chemin.

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BYNCSA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Source des images : *production personnelle*, Q. Fortier
