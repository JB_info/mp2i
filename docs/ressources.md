# Ressources supplémentaires

Vous trouverez ici des ressources supplémentaires qui vous permettront de :

* réviser les cours d'informatique de MP2I
* vous entraîner sur des exercices de programmation
* lire des informations complémentaires à la limite du programme

... mais aucune obligation de s'en servir ! Les cours, TD et TP faits en classe suffisent largement, ces ressources sont uniquement destinées à ceux qui souhaitent s'exercer encore plus où qui s'intéressent à des thèmes que l'on a pas forcément vus en cours. Vous y trouverez aussi peut-être de quoi vous aider pour vos TIPE.

*Remarque : certaines de ces ressources peuvent n'exister qu'en anglais.*

*Remarque n°2 : je déconseille fortement l'achat de livres intitulés "MP2I". Ils sont criblés d'erreurs, très incomplets et pas toujours en raccord avec le programme. Privilégiez les livres sur un thème du programme en particulier comme ceux de la liste suivante. Dans le doute, vous pouvez bien sûr me demander si une référence est bien.*

## I. Bases de programmation

Pour apprendre à programmer en OCaml ou en C, voici quelques références bibliographiques :

* Apprendre à programmer avec OCaml, Sylvain Conchon et Jean-Christophe Filiâtre, Éditions Eyrolles.
* Real World OCaml, Yaron Minsky, Anil Madhavapeddy, Jason Hickey, Éditeur O'Reilly.
* Le langage C - Norme ANSI, Brian W. Kernighan, Dennis M. Ritchie, 2ème édition (Dunod).
* Expert C Programming, Peter van der Linden, Éditeur Prentice Hall.
* Algorithmique & programmation en langage C, Damien Berthet, Vincent Labatut.

Le premier en OCaml (Conchon et Filiâtre) et le premier en C (Kernighan et Ritchie) sont ceux les plus adaptés pour vous. Les autres sont plus adaptés aux plus forts en programmation qui souhaitent aller dans le détail.

Voici un site qui propose des cours, exercices et petits problèmes en C, classés par thèmes :

* [France IOI](http://www.france-ioi.org/algo/chapters.php) : *Cochez bien "Langage C" dans le menu en haut de la page. Les exercices sont aussi disponibles en OCaml mais ne les faîtes pas : ils sont très mal adaptés.*

Voici trois sites pour OCaml :

* [OCaml.org](https://ocaml.org/exercises) : exercices accompagnés de leurs corrigés et de difficulté croissante
* [Learn-OCaml](https://ocaml-sf.org/learn-ocaml-public/#activity%3Dexercises) : exercices de difficultés variables
* [OCaml Programming](https://cs3110.github.io/textbook/cover.html) : cours avec exemples et petits exercices partiellement corrigés

Voici un site qui propose divers problèmes mathématiques dont la solution nécessite l'écriture d'un programme :

* [Projet Euler](https://projecteuler.net/archives) : vous pouvez essayer de répondre aux problèmes en C, en OCaml, ou les deux !

## II. Algorithmique

Ces livres couvrent toute l'algorithmique au programme de MP2I et bien plus encore :

* Introduction to Algorithms, Thomas H. Cormen, Charles E. Leiserson, Ronald L. Rivest, Clifford Stein (2009 pour la troisième édition, 2022 la dernière édition).
* Algorithms, Jeff Erickson, 2019.


---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)