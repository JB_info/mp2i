# TP : Graphes en C

La première partie de ce TP a pour objectif de découvrir les particularités des représentations sous forme de liste et matrice d'adjacence d'un graphe en C. La seconde partie s'intéresse aux diverses applications des parcours de graphes.

## I. Implémentations des graphes

On rappelle que l'on peut représenter un graphe par sa matrice d'adjacence :

![](../ocaml/img/matrice_dadjacence.png)

La manière la plus « simple » d'implémenter la matrice d'adjacence d'un graphe en C est d'utiliser un **tableau statique linéarisé**.

Pour avoir un tableau statique, nous devons connaître sa taille à la compilation : nous allons donc supposer avoir un nombre maximal de sommets fixé, disons 30 pour ce TP. Un graphe sera alors constitué d'un tableau staitique de ce genre, et d'un entier stockant le réel nombre ($`\leq 30`$) de sommets du graphe. 

Voici donc le type que nous utiliserons :

```c
typedef int mat_adjacence[900]; // matrice (30,30) linéarisée
struct graphe_s {
    int nb_sommets; // nombre de sommets du graphe, toujours <= 30
    mat_adjacence mat;
};
typedef struct graphe_s graphe; // ligne à placer dans le fichier d'entête comme d'habitude
```

Un exemple de graphe non orienté $`G_{NO}`$ et sa matrice d’adjacence implémentée avec ce type :

![](../ocaml/img/graphe_non_oriente.png)

```c
int main() {
    graphe gno_mat = {9,  { 0, 1, 0, 0, 1, 1, 0, 0, 0,
                            1, 0, 0, 1, 0, 0, 0, 0, 0,
                            0, 0, 0, 0, 1, 1, 1, 0, 0,
                            0, 1, 0, 0, 0, 1, 0, 0, 0,
                            1, 0, 1, 0, 0, 1, 0, 0, 1,
                            1, 0, 1, 1, 1, 0, 1, 0, 0,
                            0, 0, 1, 0, 0, 1, 0, 1, 0,
                            0, 0, 0, 0, 0, 0, 1, 0, 0,
                            0, 0, 0, 0, 1, 0, 0, 0, 0 } };
}
```

> 1. Implémentez avec ce type la matrice d'adjacence du graphe orienté $`G_{O}`$ suivant :
>
>     ![](../ocaml/img/graphe_oriente.png)

On va définir quelques fonctions sur les graphes avec ce type. N'oubliez pas qu'il ne faut pas parcourir la matrice dans son intégralité mais bien prendre en compte le nombre de sommets du graphe.

> 2. Écrivez une fonction `bool successeur(graphe g, int i, int j)` qui renvoie `true` si et seulement si le sommet `j` est un successeur du sommet `i` (ou voisin si `g` est non orienté).
>3. Écrivez une fonction qui prend en paramètre un graphe supposé orienté et un sommet et détermine son degré entrant et son degré sortant. Peut-on renvoyer un couple ? Comment faut-il alors procéder ?
> 4. Écrivez une fonction `int nb_aretes_ou_arcs(graphe g)` qui prend en paramètre un graphe et renvoie son nombre d'arêtes s'il est non orienté / d'arcs s'il est orienté. Cette fonction doit donc renvoyer 12 pour $`G_{NO}`$ et 14 pour $`G_O`$. *Vous ne devez parcourir la matrice qu'une unique fois.*
>

On rappelle qu'on peut également représenter un graphe par sa liste d'adjacence :

![](../ocaml/img/liste_dadjacence.png)

La manière la plus « simple » d'implémenter la liste d'adjacence d'un graphe en C est d'utiliser un **tableau statique bidimensionnel**. Tout comme pour les matrices, nous supposerons donc avoir un nombre maximal de sommets fixé (30 pour ce TP).

La ligne `i` du tableau bidimensionnel implémentant la liste d'adjacence contient les voisins / successeurs du `i`-ème sommet du graphe. On doit alors retenir le nombre de voisins / successeurs que possède le sommet `ì` (pour savoir où s'arrêter dans le parcours du tableau). Pour cela, deux méthodes sont possibles :

* on stocke dans la première case du tableau le nombre de voisins / successeurs ;
* on utilise une **sentinelle** qui marque la fin des voisins / successeurs.

Voici donc le type que nous utiliserons :

```c
typedef int lst_adjacence[30][31];
struct graph_s {
    int nb_sommets;
    lst_adjacence lst;
};
// graph sans 'e' pour ne pas avoir de conflit avec la représentation précédente
typedef struct graph_s graph;
```

Voici donc l'implémentation de $`G_{NO}`$ avec ce type :

```c
int main() {
    // PREMIÈRE POSSIBILITÉ : DEGRÉ (SORTANT)
    // La première case de chaque ligne i donne d(i) (ou d+(i) pour les graphes orientés).
    // Si cette case contient un entier n, on sait donc qu'on ne doit parcourir que les cases de 1 à n :
    // le tableau statique ne contient plus aucun voisin / successeur ensuite.
    graph gno_lst1 = {9,  { {3,1,4,5},
                            {2,0,3},
                            {3,4,5,6},
                            {2,1,5},
                            {4,0,2,5,8},
                            {5,0,2,3,4,6},
                            {3,2,5,7},
                            {1,6},
                            {1,4}} };

    // DEUXIÈME POSSIBILITÉ : SENTINELLE
    // La valeur -1, qui ne correspond à aucun numéro de sommet, peut par exemple servir de sentinelle.
    // Dès que l'on croise cette valeur, on sait qu'on peut arrêter le parcours :
    // le tableau statique ne contient plus aucun voisin / successeur ensuite.
    graph gno_lst2 = {9,  { {1,4,5,-1},
                            {0,3,-1},
                            {4,5,6,-1},
                            {1,5,-1},
                            {0,2,5,8,-1},
                            {0,2,3,4,6,-1},
                            {2,5,7,-1},
                            {6,-1},
                            {4,-1}} };
}
```

> 5. Implémentez avec les deux méthodes possibles la liste d'adjacence du graphe $`G_O`$.
> 6. Écrivez une fonction `bool successeur_lst(graph g, int i, int j)` qui renvoie `true` si et seulement si le sommet `j` est un successeur du sommet `i` (ou voisin si `g` est non orienté). Vous proposerez une version de cette fonction utilisant la sentinelle, et une autre utilisant le degré sortant.
> 7. Écrivez une fonction `void degres(graph g, int sommet, int* entrant, int* sortant)` qui prend en paramètre un graphe supposé orienté et un sommet et détermine son degré entrant et son degré sortant. Vous proposerez à nouveau une version de cette fonction utilisant la sentinelle, et une autre utilisant le degré sortant.

Tout comme les arbres, on peut avoir besoin de sérialiser un graphe.

> 8. Proposez une fonction `void serialise_lst(char* nom_fichier, graph g)` pour sérialiser un graphe représenté par sa liste d'adjacence (format des listes au choix), puis une pour sérialiser un graphe représenté par sa matrice d'adjacence `void serialise_mat(char* nom_fichier, graphe g)`. Les fichiers devront avoir le format suivant :
>
>     * la première ligne contient le nombre de sommets du graphe ;
>     * les lignes suivantes contiennent des couples $i, j$ signifiant qu'il y a un arc / une arête de $i$ à $j$.
>
> 9. Proposez une fonction `graphe deserialise_mat(char* nom_fichier)` pour reconstruire la matrice d'adjacence d'un graphe sérialisé.
>
> 10. Commentez la fonction suivante pour expliquer ce qu'elle fait :
>
>     ```c
>     graph deserialise_lst(char* nom_fichier)  {
>         FILE* fichier = fopen(nom_fichier, "r");
>         assert (fichier != NULL);
>         
>         int nb_sommets;
>         fscanf(fichier, "%d", &nb_sommets);
>         
>         graph g = {nb_sommets, {}};
>     	int degres[30] = {0};
>     
>         int i, j;
>         while (fscanf(fichier, "%d,%d", &i, &j) != EOF) {
>             g.lst[i][degres[i]] = j;
>             degres[i] += 1;
>         }
>     
>         for (int i = 0; i < nb_sommets; i += 1) {
>             g.lst[i][degres[i]] = -1;
>         }
>         
>         fclose(fichier);
>         return g;
>     }
>     ```
>
> 11. Modifiez la fonction précédente pour utiliser l'autre manière de stocker les listes d'adjacence.

## II. Applications des parcours de graphes

On rappelle que parcourir un graphe est une tâche assez similaire au parcours d’un arbre, avec quelques différences importantes :

* Il n’y a pas de « racine » : en règle générale, on parcourt à partir d’un sommet `x` et l’on ne parcourra bien sûr que les sommets accessibles à partir de `x`.
* Le plus important : a priori, il y a des cycles (chemins ayant le même sommet de départ et d'arrivée), et il ne faut pas tourner en rond ! D’une manière ou d’une autre, il faut donc se souvenir des sommets que l’on a déjà visités.

Voici l'algorithme pour un parcours en profondeur du graphe `G` à partir du sommet `v` :

![](../ocaml/img/graphe_parcours_profondeur.png)

Voici l'algorithme pour un parcours en largeur du graphe `G` à partir du sommet `v` :

![](../ocaml/img/graphe_parcours_largeur.png)

> **Parcours simples**
>
> 1. Quelle structure de données choisir pour `vus` ? Pourquoi ? Quelle implémentation du graphe choisir ? Pourquoi ? *Appelez le professeur pour vérifier que vous faîtes le bon choix, ce sera primordial pour la complexité.*
> 2. Implémentez un parcours en profondeur `void parcours_profondeur(graph g, int depart)`, et un parcours en largeur `void parcours_largeur(graph g, int depart)`. Le traitement consistera simplement à afficher les sommets parcourus.
> 3. Donnez la complexité de ces deux parcours.

Ces deux ordres constituent essentiellement les seules manières dont on peut avoir besoin de parcourir un graphe. Tous les algorithmes sur les graphes sont basés sur ces parcours, il ne s'agira que de les adapter légèrement pour répondre au problème posé.

> **Distances**
>
> 4. Écrivez une fonction `void distances_largeur(graph g, int depart, int distances[30])` qui prend en paramètre un graphe (orienté ou non) et un sommet de départ, et complète le tableau `distances` donné en paramètre afin qu'il contienne les distances du sommet de départ à chaque autre sommet du graphe. On pourra initialiser les cases de ce tableau à -1. Il faut adapter le parcours *en largeur*.
> 5. Pourquoi faut-il impérativement utiliser un parcours en largeur pour calculer une distance ?

> **Tri topologique**
>
> 6. Soit $x$ et $y$ deux sommets d'un graphe orienté acyclique $G = (S, A)$ tels que $`(x,y) \in A`$. Montrez que si on effectue un parcours en profondeur de $G$ (depuis n'importe quel sommet), alors l'exploration de $y$ termine avant celle de $x$.
>
> On déduit de la question précédente la propriété suivante : Pour obtenir un tri topologique d'un graphe orienté acyclique, il suffit d'ordonner les sommets par dates décroissantes de fin de parcours *en profondeur* (autrement dit, le sommet dont l'exploration s'est terminée en premier sera le dernier du tri, etc.).
>
> Pour que le tri topologique contienne bien tous les sommets du graphe, il faut lancer un parcours en profondeur depuis chaque sommet de degré entrant nul.
>
> 7. Écrivez alors une fonction `void tri_topologique(graph g, int ordre_topologique[30])` qui prend en paramètre un graphe orienté acyclique et complète le tableau `ordre_topologique` de manière à ce qu'il contienne une énumération de tous les sommets du graphe respectant l'ordre topologique.
>
>     *Indication : il peut être difficile de savoir à quel indice du tableau `ordre_topologique` écrire $s$ lorsque l'exploration d'un sommet $s$ est terminée. Pour remédier à ce problème, on pourra stocker dans une structure de données intermédiaire les sommets dont l'exploration est terminée, et remplir le tableau `ordre_topologique` uniquement à la fin (une fois que l'exploration de l'intégralité des sommets est terminée, et donc une fois que la structure intermédiaire contient tous les sommets). Pour choisir cette structure intermédiaire, on remarquera que le sommet qui sera extrait de cette structure en premier pour remplir le tableau, devra être celui qui y a été ajouté en dernier...*

Comme le montre les applications aux parcours de graphes précédentes, il arrive régulièrement qu'un parcours de graphe dans un certain ordre soit préférable à l'autre (voire même que ce soit la seule option). D'autres fois, les deux sont équivalents. *À partir de maintenant, ce sera à vous de choisir quel parcours adapter pour répondre au problème.*

> **Autres applications**
>
> 8. Écrivez une fonction `bool connexe(graph g)` qui détermine si un graphe non orienté est connexe.
> 8. Écrivez une fonction `bool biparti(graph g)` qui détermine si un graphe non orienté connexe est biparti.
> 9. Écrivez une fonction `void composantes_connexes(graph g, int comp[30])` qui trouve les composantes connexes d'un graphe non orienté, sous forme d'un tableau statique `comp` d'entiers correspondants à la composante connexe à laquelle appartiennent les sommets. Ainsi `comp[i] == comp[j]` si et seulement si les deux sommets `i` et `j` appartiennent à la même composante connexe.
> 10. Écrivez une fonction `bool cyclique(graph g)` qui détermine si un graphe non orienté est cyclique. On pourra commencer par une fonction qui détermine s'il existe un cycle avec sommet de départ fixé.
> 11. Déduisez des fonctions précédentes une fonction `bool est_arbre(graph g)` qui détermine si un graphe est un arbre (tel que défini dans la théorie des graphes).

On peut construire un arbre enraciné en effectuant un parcours (en largeur ou en profondeur) d'un graphe connexe, on appelle cela l'*arborescence d'un parcours*. Soit $G = (S, A)$ un graphe non orienté connexe et `v` le sommet de départ du parcours : l'arborescence du parcours de $G$ depuis `v` est le graphe $G' = (S, A')$ où $`(x,y) \in A'`$ si et seulement si le sommet `y` a été exploré à partir du sommet `x`.

Voici un exemple d'arborescence d'un parcours en profondeur du graphe non orienté suivant à partir du sommet 5 :

![](img/arborescence_parcours.png)

> **Arborescence d'un parcours**
>
> 13. Donnez l'arborescence d'un parcours en profondeur du graphe $`G_{NO}`$​ à partir du sommet 0. Même question pour un parcours en largeur.
> 14. Écrire une fonction `graph arborescence_largeur(graph g, int depart)` qui construit et renvoie l'arborescence d'un parcours en largeur d'un graphe non orienté connexe.
> 15. Même question pour un parcours en profondeur.

## Pour aller plus loin

> On peut implémenter un parcours en profondeur impératif à l'aide d'une pile. Essayez d'adapter vos algorithmes afin de réaliser ce parcours.


---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Sources des images : *production personnelle*, Q. Fortier (matrice et liste d'adjacence), J.B. Bianquis (arborescence)
