# TP : Arbres binaires de recherche (ABR) et arbres bicolores (ARN)

***Ce TP est à faire en C.***

Nous allons découvrir ici quelques arbres binaires particuliers : les ABR ; et leur version équilibrée, les ARN.

## I. Arbres binaires de recherche

Un **arbre binaire de recherche** (ABR) est un arbre binaire non strict qui respecte la propriété suivante : pour tout nœud de l'arbre, l'étiquette du nœud est supérieure à toutes les étiquettes des nœuds de son sous-arbre gauche et inférieure à toutes celles de son sous-arbre droit.

L'ensemble des étiquettes de l'arbre doit donc être muni d'une relation d'ordre totale.

Dans cette partie, on se servira de cet ABR comme exemple :

![](img/abr_arn/abr.png)

### 1. Implémentation d'un ABR

Pour simplifier, on supposera dans ce TP que :

* toutes les étiquettes des arbres sont deux à deux distinctes ;
* toutes les étiquettes des arbres sont des entiers.

> 1. Dessinez tous les arbres binaires de recherche possibles dont l'ensemble des étiquettes est {7, -2, 4, 0}.
> 3. Proposez une définition inductive des ABR.
> 3. Créez une structure pour représenter un nœud d'un arbre. On supposera qu'un nœud connaît son étiquette, son fils droit, son fils gauche et *son père*. Un arbre `abr` sera alors un pointeur vers sa racine.
> 5. Écrivez une fonction `abr construit(int e, abr g, abr d)` qui renvoie un `abr` dont la racine a pour étiquette `e`, fils gauche `g` et fils droit `d`.
> 6. Écrivez une fonction `void detruit(abr a)` pour libérer la mémoire associée au nœud `a` et ses sous-arbres.

Voici l'implémentation de l'ABR donné en exemple plus haut :

```c
int main() {
    abr trois = construit(3, NULL, NULL);
    abr sept = construit(7, NULL, NULL);
    abr six = construit(6, NULL, sept);
    abr dix = construit(10, six, NULL);
    abr douze = construit(12, dix, NULL);
    abr cinq = construit(5, trois, douze);
    abr dix_huit = construit(18, NULL, NULL);
    abr vingt_trois = construit(23, NULL, NULL);
    abr vingt = construit(20, dix_huit, vingt_trois);
    abr seize = construit(16, NULL, vingt);
    abr quinze = construit(15, cinq, seize);
    
    detruit(quinze);
}
```

### 2. Recherche dans un ABR

Pour effectuer une recherche dans un ABR, il suffit de procéder ainsi :

* si l'arbre est vide, terminer la recherche par un échec
* sinon, comparer l'étiquette recherchée à celle de la racine puis :
    * si c'est égal, terminer la recherche par un succès
    * si l'étiquette recherchée est inférieure, continuer la recherche dans le sous-arbre gauche
    * sinon, continuer la recherche dans le sous-arbre droit

> 6. Déroulez cet algorithme pour rechercher 6 dans l'arbre donné en exemple plus haut. Même question pour rechercher 19.
> 7. Implémentez une fonction récursive `bool recherche_abr(abr, int)`.
> 8. Implémentez une version impérative `bool recherche_abr_imperatif(abr, int)` de cet algorithme.

Les arbres binaires de recherche peuvent être utilisés pour implémenter des tableaux associatifs.

> 9. Justifiez que la recherche d'une étiquette est en $`\mathcal O(h)`$, avec $`h`$​ la hauteur de l'ABR, et donnez un encadrement pour la hauteur $`h`$ d'un ABR (non vide) à $`n`$​ nœuds.
> 10. Comparez alors l'efficacité du test de présence d'une clé dans un tableau associatif implémenté par une table de hachage / par un ABR.

On peut facilement retrouver le minimum et le maximum dans un ABR.

> 11. Où se trouve le maximum dans un ABR ? le minimum ? Écrivez alors efficacement deux fonctions `int minimum_abr(abr)` et `int maximum_abr(abr)`.

### 3. Insertion dans un ABR

L'insertion dans un ABR se fait toujours aux feuilles. On recherche l'endroit où insérer la nouvelle étiquette de la même manière que pour la recherche décrite ci-dessus.

> 12. Expliquez où serait inséré l'étiquette 13 dans l'arbre donné en exemple ci-dessus.
>
> 11. Proposez un ordre d'insertion des étiquettes qui permettrait, à partir de l'arbre vide, de recréer l'arbre donné en exemple plus haut.
>
> 12. Complétez la fonction d'insertion suivante. Si l'étiquette est déjà présente dans l'arbre, il ne doit pas être modifié.
>
>     ```c
>     // Insertion par effet de bord de l'étiquette e dans l'ABR (*a)
>     void insertion_abr(abr* a, int e) {
>                                                                 
>         // si l'arbre était vide, il devient une feuille d'étiquette e
>         if (*a == NULL) {
>             *a = construit(/* A COMPLETER */);
>         }
>                                                                 
>         // si l'insertion de e doit se faire à droite
>         else if ((*a)->etiq < e) {
>             // si (*a) n'a pas de sous-arbre droit, on insère ici
>             if ((*a)->droit == NULL) {
>                 (*a)->droit = construit(/* A COMPLETER */);
>                 (*a)->droit->pere = /* A COMPLETER */;
>             }
>             // sinon on insère récursivement dans le sous-arbre droit
>             else {
>                 insertion_abr(/* A COMPLETER */, e);
>             }
>         }
>                                                                 
>         // si l'insertion de e doit se faire à gauche
>         /* A COMPLETER */
>     ```

On peut utiliser un arbre binaire de recherche pour réaliser un algorithme de tri.

> 15. Quel type de parcours permet de récupérer les étiquettes d'un ABR dans l'ordre croissant ? Déduisez-en une fonction `void tri_via_abr(int*, int)` qui prend en paramètre un tableau (et sa taille), et passe par un arbre binaire de recherche pour afficher ses éléments dans l'ordre croissant. La fonction ne doit pas avoir d'effet de bord.
> 14. Donnez un exemple de tableau pour lequel le tri est le plus efficace, et un pour lequel il est le moins efficace. Quelle est la complexité dans le meilleur cas ? le pire cas ?

### 4. Suppression dans un ABR

Une première manière de supprimer un nœud d'un ABR est de procéder par *fusion*. On descend dans l'arbre comme pour la recherche, jusqu'à trouver le nœud `z` à supprimer. En supprimant ce nœud `z`, ses deux sous-arbres se retrouvent alors détachés. On les fusionne comme décrit par le schéma ci-contre, puis on rattache le tout au père de `z` :

![](img/abr_arn/suppression_fusion.png)

> 17. Justifiez que cette méthode produit bien un ABR.
>
> 18. Dessinez l'arbre produit en supprimant le nœud d'étiquette 15 de l'arbre donné en exemple plus haut.
>
> 19. Donnez à l'aide d'un schéma une méthode de fusion similaire qui placerait $`x_2`$​ comme racine.
>
> 20. Écrivez une fonction `abr fusion(abr x1, abr x2)` qui prend en paramètre deux arbres binaires de recherche et les fusionne comme décrit par le schéma ci-dessus.
>
> 21. Écrivez une fonction `abr* noeud_a_supprimer(abr* a, int e)` qui recherche et renvoie un pointeur vers l'ABR de racine `e`. S'il n'y en a aucun, on renverra `NULL`.
>
> 22. Complétez la fonction `void suppression_abr_fusion(abr*, int)` qui supprime le nœud dont l'étiquette est passée en paramètre :
>
>     ```c
>     void suppression_abr_fusion(abr* a, int e) {
>         // on récupère le nœud à supprimer
>         abr a_supprimer = *noeud_a_supprimer(a, e);
>         if (a_supprimer != NULL) {
>             abr pere_du_noeud_supprime = /* A COMPLETER */;
>                 
>             // on fusionne ses fils, et on les raccroche dans l'arbre
>             abr f = fusion(/* A COMPLETER */);
>             if (f != NULL) {
>                 f->pere = /* A COMPLETER */;
>             }
>                 
>             // si on a supprimé la racine, la fusion des fils devient notre nouvelle racine
>             if (pere_du_noeud_supprime == NULL) {
>                 *a = /* A COMPLETER */;
>             }
>             else {
>                 // sinon on rattache la fusion des fils au père du bon côté
>                 if (pere_du_noeud_supprime->etiq < e) {
>                     pere_du_noeud_supprime->/* A COMPLETER */ = f;
>                 }
>                 else {
>                     /* A COMPLETER */
>                 }
>             }
>                 
>             // on libère le nœud
>             /* A COMPLETER */
>         }
>     }
>     ```

Une seconde manière de supprimer un nœud est de procéder par *remontée du minimum*. Supposons qu'on souhaite supprimer un nœud `z`. Il y a trois cas à distinguer :

* si `z` est une feuille, la suppression se fait directement :

    ![](img/abr_arn/suppression_remontee_min_1.png)

* si `z` n'a qu'un seul fils, il suffit de relier ce fils au parent de `z` :

    ![](img/abr_arn/suppression_remontee_min_2.png)

* si `z` a deux fils, on remplace `z` par le minimum de son sous-arbre droit (noté `y`) en recopiant l'étiquette de `y` dans `z` puis en supprimant `y`. Comme `y` est le minimum d’un sous-arbre, son fils gauche est nécessairement vide : on se retrouve dans le deuxième cas que l’on sait gérer.

    ![](img/abr_arn/suppression_remontee_min_3.png)

> 23. Expliquez comment serait supprimé le nœud d'étiquette 15 dans l'arbre donné en exemple.
>
> 24. Justifiez que dans le troisième cas (deux fils non vides), on pourrait également remplacer le nœud `z` par le maximum de son sous-arbre gauche.
>
> 25. *Sauf si avancez vite, je vous conseille de faire la partie II. du TP et de revenir ensuite sur cette question.*
>
>     La fonction suivante implémente la suppression par remontée du minimum :
>
>     ```c
>     void suppression_abr_remontee_minimum(abr* a, int e) {
>         // on recherche le nœud z à supprimer
>         abr* z = noeud_a_supprimer(a, e);
>         if (*z != NULL) {
>             // 1er cas : z est une feuille
>             if ((*z)->gauche == NULL && (*z)->droit == NULL) {
>                 supprime_feuille(z);
>             }
>             // 2ème cas : z a un seul fils
>             else if ((*z)->gauche == NULL || (*z)->droit == NULL) {
>                 supprime_noeud_a_un_fils(z);
>             }
>             // 3ème cas : z a deux fils
>             else {
>                 supprime_noeud_a_deux_fils(z);
>             }
>         }
>     }
>     ```
>
>     Implémentez les trois fonctions `void supprime_feuille(abr*)`, `void supprime_noeud_a_un_fils(abr*)` et `void supprime_noeud_a_deux_fils(abr*)` qui suppriment la racine de l'arbre passé en paramètre, en vous aidant des schémas ci-dessus.

## II. Arbres bicolores

Pour remédier aux problèmes d'efficacité des ABR lorsqu'ils ne sont pas équilibrés, nous allons voir dans cette partie suivante une autre structure : les arbres bicolores, aussi appelés arbres rouge-noir (ARN).

### 1. Implémentation des ARN

Chaque nœud d'un arbre bicolore possède une couleur : rouge ou noir.

En C, on utilisera un champ supplémentaire de type `couleur` :

```c
enum couleur_e {rouge, noir};
typedef enum couleur_e couleur;
```

> 1. Créez une structure pour représenter un nœud d'un arbre bicolore. On supposera qu'un nœud connaît son étiquette, son fils droit, son fils gauche, et sa couleur. Un arbre bicolore `arn` sera alors un pointeur vers sa racine.
> 1. Écrire deux fonctions `arn construit_arn(int e, couleur c, arn g, arn d)` et `void detruit_arn(arn a)`.

Voici les propriétés d'un arbre bicolore :

* un arbre bicolore est un ABR ;
* la racine est noire ;
* un nœud rouge ne peut pas avoir de fils rouge ;
* tous les chemins de la racine à un nœud vide contiennent le même nombre de nœuds noirs ;

Voici un exemple d'arbre bicolore (non étiqueté) :

![](img/abr_arn/arn_non_etiquete.png)

> 3. Proposez un étiquetage de cet arbre avec les entiers naturels entre 0 et 13 qui respecte les propriétés des ABR.

Les arbres bicolores sont considérés comme *équilibrés* : le chemin le plus long possible d'une racine à une feuille (la hauteur de l'arbre) ne peut être que deux fois plus long que le plus petit possible. Dans le cas le plus déséquilibré, le plus court des chemins ne comporte que des nœuds noirs, et le plus long alterne les nœuds rouges et noirs.

> 4. Colorez l'ABR suivant afin qu'il respecte les propriétés des arbres bicolores :
>
>     ![](img/abr_arn/arn_non_colore.png){width=20%}
>
> 4. Implémentez alors cet arbre bicolore en C en utilisant la fonction `construit_arn`.
>
> 5. Est-il possible de colorer l'ABR suivant ?
>
>     ![](img/abr_arn/abr.png)

### 2. Insertion dans un ARN

Lors de l'insertion ou de la suppression d'une étiquette, il faut s'assurer de conserver l'équilibre de l'arbre. Pour cela, le ré-équilibrage sera basé sur des *rotations* :

![](img/abr_arn/rotations.png)

> 7. Justifiez qu'une rotation conserve bien les propriétés d'un ABR.
> 8. Écrivez une fonction `arn rotation_droite(arn x)` qui prend en paramètre un arbre tel que schématisé ci-dessus et lui applique une rotation droite. Aucun nouveau nœud ne doit être créé, il ne s'agit que de modifications de pointeurs.
> 9. Écrivez de même une fonction `arn rotation_gauche(arn y)`.

Pour l'insertion dans un ARN, on insérera aux feuilles comme dans les ABR, et la feuille ajoutée sera rouge. Mais si le père de cette feuille était rouge, on se retrouve alors avec un problème car l'arbre ne respecte plus les propriétés d'un arbre bicolore. Voici les 4 cas problématiques possibles et comment les régler :

![](img/arn_insertion.png)

> 10. Écrivez une fonction `arn corrige_rouge(arn a)` qui prend en paramètre un nœud, et corrige les couleurs si nécessaire. Il faut :
>     * Vérifier si on se trouve dans un des 4 cas problématiques (par exemple, si `a` est noir, que son fils gauche est rouge, et que le fils gauche de son fils gauche est rouge, on est dans le premier cas).
>     * Si on est dans des 4 cas problématiques :
>         * Appliquer les rotations nécessaires (par exemple si on est dans le premier cas problématique, une rotation à droite suffit).
>         * Colorer l'arbre obtenu correctement.
>     * Renvoyer l'arbre obtenu (ou `a` si on n'était pas dans un cas problématique).

À nouveau, si le père de `z` / `x`  (devenu père de `y`) était rouge, il  faut propager la recoloration. Pour cela, après chaque appel récursif effectué pour l'insertion, il faudra nécessairement appeler `corrige_rouge`.

Pour finir, si la racine de l'arbre se retrouve rouge, il faut la colorer en noir. À la fin de l'insertion, il faudra donc colorer la racine en noir.

> 11. Écrivez une fonction `void insertion_arn(arn* a, int e)` qui insére une étiquette dans un ARN. Vous pouvez vous inspirer de l'insertion dans un ABR, en n'oubliant pas de corriger les couleurs.

La suppression est nettement plus difficile, on n'attend pas de vous de savoir l'implémenter.

## Pour aller plus loin

Un *arbre AVL* (le nom vient de ses créateurs G. Adelson-Velsky et E. Landis) est un ABR tel que pour tout nœud, ses sous-arbres gauche et droit ont la même hauteur, à 1 près. Cette propriété assure que la hauteur de l'arbre est désormais toujours logarithmique en sa taille, et donc qu'on est toujours dans le meilleur cas pour la recherche d'une étiquette. Les arbres AVL ne sont pas au programme, mais leurs opérations sont basées sur un principe similaire à ceux des ARN (les rotations) et ils sont déjà tombés aux concours.

> 1. Proposez une définition inductive d'un arbre AVL.
> 2. Parmi les ABR dont l'ensemble d'étiquettes est {7, -2, 4, 0}, lesquels sont des arbres AVL ?

Lors de l'insertion ou de la suppression d'une étiquette, il faut s'assurer de conserver l'équilibre de l'arbre. Pour cela, le ré-équilibrage sera basé sur des rotations (comme pour les ARN). Pour éviter de recalculer les hauteurs à chaque fois, on les stockera directement dans chaque nœud de l'arbre.

> 3. Créez une structure pour représenter un nœud d'un arbre AVL. On supposera qu'un nœud connaît son étiquette, son fils droit, son fils gauche, et sa hauteur. Un arbre sera alors un pointeur vers sa racine.
> 4. Écrivez une fonction `rotation_droite` qui réalise une rotation droite. Attention à bien mettre à jour les hauteurs nécessaires. Écrivez de même une fonction `rotation_gauche`.

Si l'arbre est déséquilibré, comme l'insertion et la suppression font varier les hauteurs des sous-arbres d’au plus 1, c’est qu’un nœud (dont le sous-arbre associé a pour hauteur h) possède deux sous-arbres de hauteurs respectives h − 1 et h − 3.

On suppose que le sous-arbre de hauteur h − 1 est celui de gauche, l'autre cas s'en déduit par symétrie.

Il y a deux cas à distinguer :

* si $`h(\alpha) \geq h(\beta) \text{ (i.e. } h(\alpha)=h-2 \text{ et } h(\beta)=h-2-p \text{ avec }p\in \{0;1\})`$, une rotation droite suffit :

    ![](img/avl_equilibrage1.png)

* si $`h(\alpha) < h(\beta) \text{ (i.e. } \beta \text{ a pour sous-arbres gauche et droit } \delta, \varepsilon \text{ tels que } h(\delta)=h-3-p \text{ et } h(\varepsilon)=h-3-q \text{ avec }p,q\in \{0;1\})`$, une rotation gauche du sous-arbre gauche nous ramène (presque) au cas précédent puis une rotation droite ré-équilibre l'arbre :

    ![](img/avl_equilibrage2.png)

> 5. Écrivez une fonction qui rééquilibre un AVL (il faut comparer les hauteurs du sous-arbre gauche et du sous-arbre droit de la racine, et si l'écart de hauteur est de 2, appliquer les rotations décrites ci-dessus).
> 6. Écrivez une fonction pour insérer un élément dans un arbre AVL. Il faut effectuer l'insertion de manière récursive à droite ou à gauche comme pour les ABR, puis ré-équilibrer l'arbre.
> 7. Écrivez une fonction pour supprimer un élément dans un arbre AVL. Il faut effectuer la suppression avec la méthode de remontée du minimum, puis effectuer le ré-équilibrage.


---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Sources des images : *production personnelle*, A. Lick (ABR), J.B. Bianquis (ARN).
