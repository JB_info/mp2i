# TP : Bases de données (SQL)

Ce TP a pour objectif d'introduire la notion de *base de données relationnelle* et de présenter le langage *SQL*.

**À l'issue de ce TP, vous devez être en mesure d'écrire des requêtes SQL utilisant l'ensemble des mots-clefs présentés.** Dans quasiment tous les sujets de concours, on vous demande d'écrire 4 / 5 requêtes SQL : c'est un incontournable et ce sont des points facilement gagnés.

Contrairement à la plupart des notions abordées cette année, les bases de données ne sont pas revues en MPI : ce TP est donc le seul que vous aurez sur le sujet, *il est impératif de le terminer jusqu'au bout*.

On utilisera le logiciel [DB Browser for SQLite](https://sqlitebrowser.org/dl/), qui a l'avantage d'être libre, téléchargeable sur la plupart des systèmes d'exploitation, et permet de visualiser les données de la base afin de vérifier à la main les résultats des requêtes.

## I. Découverte du modèle relationnel

De nombreux programmes en informatique manipulent d'énormes quantités de données. Dès lors, il est nécessaire de pouvoir gérer ces données de manière efficace. C'est le travail des **S**ystème de **G**estion de **B**ase de **D**onnées (SGBD).

Les SGBD s'occupent de tout pour nous : on n'a qu'à leur demander ce qu'on souhaite faire ! C'est le principe du **paradigme logique** : un programme est une description des propriétés que doit satisfaire le résultat, et on ne fournit aucune indication sur la manière de calculer ce résultat. Par exemple, on dit au SGBD : « Trie mes données. », mais on n'explicite pas l'algorithme de tri... C'est lui qui fait tout !

Les bases de données peuvent être construites selon différent modèles. Le plus utilisé est le **modèle relationnel**. Voici son fonctionnement :

* Une base de données relationnelle est constituée de plusieurs **tables**.
* Une table est un tableau à 2 dimensions dont :
    * chaque ligne correspond à une donnée ;
    * chaque colonne correspond à une caractéristique de la donnée, appelée **attribut**.
* Chaque attribut est de type `TEXT`, `INTEGER` ou `REAL`.

Voici un exemple de table appelée *Oeuvres* représentant des peintures et quelques données appartenant à cette table :

|  id  |           nom            |     peintre      |
| :--: | :----------------------: | :--------------: |
|  1   | Autoportrait de Van Gogh |     van Gogh     |
|  2   | Le Déjeuner sur l'herbe  |      Manet       |
|  3   |        La Joconde        | Léonard de Vinci |
|  4   |   Le Jugement dernier    |   Michel-Ange    |
|  5   |  Tête d'un homme barbu   |     Picasso      |
| ...  |           ...            |       ...        |

> 1. Quels sont les 3 attributs ? Quels sont leurs types ?

Chaque ligne d'une table doit être **unique**. Dans une table, une **clé primaire** est un attribut (ou groupe d'attributs) qui identifie de manière unique une ligne (et qui garantit donc qu'il n'y aura pas de doublons).

Pour la table *Oeuvres* ci-dessus, la clé primaire est l'attribut `id` (donc deux lignes ne peuvent pas avoir la même valeur dans la colonne `id`).

Voici une seconde table appelée *Musees* :

| numero |        nom        |  ville  |
| :----: | :---------------: | :-----: |
|   1    |  Musée du Louvre  |  Paris  |
|   2    |   Musée d'Orsay   |  Paris  |
|   3    | Musées du Vatican |  Rome   |
|   4    | National Gallery  | Londres |
|  ...   |        ...        |   ...   |

> 2. Quelle est sa clé primaire ?

Pour faire le lien entre deux tables, on utilise une **clé étrangère**. Une clé étrangère est un attribut (ou groupe d'attributs) d'une table qui **référence**, c'est à dire *fait le lien avec*, une clé primaire d'une autre table.

Par exemple, si on veut indiquer dans quel musée sont exposées nos œuvres, on ajoute une clé étrangère `numero_musee` dans la table *Oeuvres*, qui fait référence à la clé primaire `numero` de la table *Musees* :

|  id  |           nom            |     peintre      | numero_musee |
| :--: | :----------------------: | :--------------: | :----------: |
|  1   | Autoportrait de Van Gogh |     van Gogh     |      2       |
|  2   | Le Déjeuner sur l'herbe  |      Manet       |      2       |
|  3   |        La Joconde        | Léonard de Vinci |      1       |
|  4   |   Le Jugement dernier    |   Michel-Ange    |      3       |
|  5   |  Tête d'un homme barbu   |     Picasso      |    `NULL`    |
| ...  |           ...            |       ...        |     ...      |

> 3. Quel est donc le nom du musée dans lequel est exposée La Joconde ?

La valeur `NULL` signifie qu'on a aucune information pour cet attribut pour cette ligne.

Un ensemble de tables, reliées entre elles au moyen de clé étrangères, forme ce qu'on appelle un **schéma relationnel**.

Voici celui de notre base :

![](img/schema_relationnel.png)

> 4. Récupérer le fichier contenant [la base de données des peintures](./code/peinture.db).
> 5. Ouvrir cette base avec DB Browser for SQLite. Avant tout, aller dans l'onglet "Éditer les Pragmas" et vérifier que "Foreign Keys" est bien coché.
> 6. Aller dans l'onglet "Parcourir les données". Vous trouverez toutes les données de la base.

Si on souhaitait trouver quelques informations dans notre base de données, ce serait long de le faire à la main. Nous allons utiliser le langage **SQL** (Structured Query Langage). Comme précisé plus haut, SQL respecte le paradigme logique (on s'occupe du "quoi" mais pas du "comment").

## II. Découverte des requêtes SQL

Nous allons écrire des **requêtes** en SQL pour récupérer des informations de la base. Les requêtes de cette partie se feront toutes sur la base de données *peintures.db* découvertes ci-dessus, via le SGBD DB Browser for SQLite.

> 0. Aller dans l'onglet "Exécutez le SQL". Pour écrire un commentaire en SQL, on précède la ligne de deux tirets. Tester :
>
>     ```sql
>     -- II. Découverte des requêtes SQL
>     ```
>
>     Vous pourrez exécuter vos requêtes en cliquant sur le triangle au dessus ou en appuyant sur F5.
>
>     *Remarque : DB Browser* offre la possibilité d'enregistrer dans un fichier les requêtes écrites, vous pourrez donc écrire toutes vos requêtes les unes à la suite des autres et télécharger votre travail à la fin.

**Quelques remarques sur la syntaxe SQL :**

* traditionnellement les noms des attributs sont entièrement en minuscules, et les noms des tables commencent par une majuscule et la suite est en minuscule ;
* les mots-clefs de SQL sont intégralement en majuscule ;
* on évite les lettres accentuées et les caractères spéciaux ;
* si vous souhaitez enchaîner plusieurs requêtes dans le même fichier, il faudra les terminer par un point-virgule.

### 1. Projection simple

Les seules requêtes requêtes au programme de MP2I sont les requêtes de **sélection**, qui permettent de récupérer des données dans la base.

Une requête de sélection commence toujours pas le mot clef `SELECT` suivi des informations à récupérer puis du mot-clef `FROM` suivi de la table dans laquelle chercher ces données.

> 1. Copier la requête suivante dans DB Browser et l'exécuter. Qu'obtenez-vous ?
>
>     ```sql
>     -- 1. projection simple
>     
>     -- q1
>     SELECT prenom, nom
>     FROM Peintres
>     ```
>
> 2. Compléter la requête suivante pour obtenir tous les noms des musées de la base :
>
>     ```sql
>     -- q2
>     SELECT ........
>     FROM ........
>     ```

Tout au long du TP vous pouvez aisément *vérifier vos résultats* en retournant dans l'onglet "Parcourir les données".

Les clés primaires sont uniques et n'ont donc pas de doublons dans une table. Cependant les autres attributs peuvent être en double ou plus, si on veut ne les afficher qu'une fois on doit utiliser le mot-clef `DISTINCT` devant l'attribut considéré.

> 3. Tester la requête suivante :
>
>     ```sql
>     -- q3
>     SELECT id_pays
>     FROM Villes
>     ```
>
> 5. Comparer le résultat avec la requête suivante :
>
>     ```sql
>     -- q4
>     SELECT DISTINCT id_pays
>     FROM Villes
>     ```

Si on souhaite récupérer tous les attributs d'une table, on peut utiliser le symbole `*` plutôt que de lister à la main les attributs.

> 5. Tester :
>
>     ```sql
>     -- q5
>     SELECT *
>     FROM Oeuvres
>     ```
>     
>     *Par défaut si rien ne vous est précisé, c'est qu'on attend l'affichage de tous les attributs.*

On peut effectuer des calculs sur les différents attributs affichés.

On dispose des opérateurs suivants sur les `INTEGER` et les `REAL` :

* `+` (addition)
* `-` (soustraction)
* `*` (multiplication)
* `/` (division entière pour `INTEGER`, flottante pour `REAL`)
* `%` (modulo)
* `ABS` (valeur absolue)

On dispose des opérateurs suivants sur les `TEXT` :

* `||` (concaténation)
* `LENGTH` (longueur)
* `LOWER` / `UPPER` (mise en minuscule / majuscule)

> 6. Que fait la requête suivante ?
>
>     ```sql
>     -- q6
>     SELECT LOWER(prenom) || ' ' || UPPER(nom), annee_mort - annee_naissance
>     FROM Peintres
>     ```

Les lignes que l'on récupère sont toujours données dans un ordre arbitraire, et les colonnes ont parfois un nom non significatif, nous allons voir comment formater un peu ce résultat.

### 2. Formater le résultat

On peut si on le souhaite renommer un attribut ou une table lors d'une requête à l'aide du mot-clef `AS`.

> 1. Tester la requête suivante :
>
>     ```sql
>     -- 2. formater le résultat
>     
>     -- q1
>     SELECT nom, id_peintre AS 'Identifiant du peintre'
>     FROM Oeuvres
>     ```
>
> 2. On peut également renommer une table :
>
>     ```sql
>     -- q2
>     SELECT nom
>     FROM Oeuvres AS O
>     ```
>
>     Pas très utile ici, mais peut servir si on a besoin d'utiliser le nom de la table dans la suite de la requête (ce qui sera souvent le cas) et qu'il est trop long.
>
> 3. Modifier la requête suivante afin de renommer les deux colonnes de manière appropriée :
>
>     ```sql
>     -- q3
>     SELECT LOWER(prenom) || ' ' || UPPER(nom), annee_mort - annee_naissance
>     FROM Peintres
>     ```

Le mot-clef `ORDER BY` permet de trier les résultats d'une requête dans l'ordre croissant (par défaut ou si suivi de `ASC`) ou dans l'ordre décroissant (si suivi de `DESC`).

> 4. Tester les requêtes suivantes :
>
>     ```sql
>     -- q4
>     SELECT *
>     FROM Villes
>     ORDER BY nom
>     ```
>
>     ```sql
>     SELECT *
>     FROM Villes
>     ORDER BY nom ASC
>     ```
>
>     ```sql
>     SELECT *
>     FROM Villes
>     ORDER BY nom DESC
>     ```
>
>     *Remarque : les mots-clefs ne peuvent pas aller n'importe où dans la requête, il y a un ordre.*
>
> 6. Écrire une requête pour obtenir les noms des peintres de celui ayant vécu le plus longtemps à celui ayant vécu le moins longtemps.

Il est également possible de ne conserver qu'une partie des résultats. Pour cela, on utilise les mots-clefs `LIMIT` suivi du nombre de lignes à garder puis éventuellement de `OFFSET` suivi du nombre de premières lignes à ignorer.

> 6. Identifier ce que fait cette requête, puis l'exécuter pour vérifier :
>
>     ```sql
>     -- q6
>     SELECT *
>     FROM Villes
>     ORDER BY LENGTH(nom) ASC
>     ```
>
> 8. Tester ensuite la requête suivante :
>
>     ```sql
>     -- q7
>     SELECT *
>     FROM Villes
>     ORDER BY LENGTH(nom) ASC
>     LIMIT 5
>     ```
>
> 9. Tester pour finir la requête suivante :
>
>     ```sql
>     -- q8
>     SELECT *
>     FROM Villes
>     ORDER BY LENGTH(nom) ASC
>     LIMIT 5
>     OFFSET 2
>     ```

Deux remarques sur l'utilisation de ces deux mots-clefs :

* On peut utiliser `LIMIT` sans mettre de `OFFSET` ensuite.
* On ne peut pas utiliser `OFFSET` sans `LIMIT` avant.

> 9. Écrire une requête qui affiche le nom du peintre qui est mort il y a le moins longtemps.
> 10. Écrire une requête qui affiche les noms et années de mort des 5 peintres qui sont morts il y a le plus longtemps. Attention, il faut ignorer le peintre qui n'est pas encore mort...

### 3. Sélection simple

Pour ne sélectionner des lignes que si elles respectent une certaine condition, on utilise le mot-clef `WHERE`. Il se place après le `FROM` et avant les formatages.

On peut :

* comparer des données avec les opérateurs : `=`, `<>`, `>`, `>=`, `<`, `<=`
* tester si une donnée est vide ou non avec `IS NULL` et `IS NOT NULL` (l'opérateur `=` ne fonctionne pas pour `NULL`)
* relier des conditions grâce aux opérateurs booléens `AND`, `OR`, `NOT`.

> 1. Tester la requête suivante :
>
>     ```sql
>     -- 3. selection simple
>     
>     -- q1
>     SELECT *
>     FROM Oeuvres
>     WHERE id_peintre = 180
>     ```
>
> 2. Tester la requête suivante :
>
>     ```sql
>     -- q2
>     SELECT nom_commun
>     FROM Peintres
>     WHERE nom_commun IS NOT NULL
>     ```
>
> 3. Tester la requête suivante :
>
>     ```sql
>     -- q3
>     SELECT nom_commun AS 'Noms communs d''au moins 15 caractères'
>     FROM Peintres
>     WHERE nom_commun IS NOT NULL AND LENGTH(nom_commun) >= 15
>     ```
>
> 5. Écrire en nommant les colonnes de manière appropriée une requête permettant d'obtenir l'âge et le nom complet (prénom et nom concaténés) des peintres encore en vie dont l'identifiant du pays est entre 2 et 5.

Sur le domaine `TEXT`, il est en plus possible de comparer deux chaînes à l'aide du mot-clef `LIKE` et de deux symboles :

* `_` remplace n'importe quel caractère ;
* `%` remplace un nombre quelconque de caractères.

> 5. Tester :
>
>     ```sql
>     -- q5
>     SELECT *
>     FROM Oeuvres
>     WHERE nom LIKE 'Le%'
>     ```
>
> 7. Ajouter un espace avant le "%" et comparer les résultats.
>
> 8. Compléter la requête suivante, permettant d'afficher le nom des œuvres actuellement exposées dans un musée et dont le nom comporte deux 'e' encadrant exactement 2 caractères :
>
>     ```sql
>     -- q7
>     SELECT nom
>     FROM Oeuvres
>     WHERE id_musee ..... NULL AND nom LIKE .....
>     ```

### 4. Fonctions d'agrégation

Les fonctions qui utilisent toutes les valeurs des lignes sélectionnées pour renvoyer un seul résultat sont appelées les fonctions d'agrégation. Celles au programme sont `MAX`, `MIN`, `SUM`, `AVG`, `COUNT`.

> 1. Tester la requête suivante :
>
>     ```sql
>     -- 4. fonctions d'agrégation
>     
>     -- q1
>     SELECT prenom, nom, MIN(annee_naissance) AS 'année naissance du plus vieil auteur'
>     FROM Peintres
>     ```
>
> 2. Compléter la requête suivante, qui renvoie la date de mort du peintre mort le plus récemment :
>
>     ```sql
>     -- q2
>     SELECT .....
>     FROM Peintres
>     ```
>
> 3. Comment calculer la moyenne des âges qu'avaient les peintres à leur mort ?
>
> 4. Que fait la requête suivante ?
>
>     ```sql
>     -- q4
>     SELECT COUNT(id_musee)
>     FROM Oeuvres
>     ```
>
>     Comparer le résultat avec la requête suivante :
>
>     ```sql
>     SELECT COUNT(DISTINCT id_musee)
>     FROM Oeuvres
>     ```
>
>     Laquelle de ces deux requêtes permet de déterminer le nombre de musées qui exposent des œuvres ?
>
> 5. Déterminer le nombre de villes possédant un musée.
>
> 6. Que fait la requête suivante ?
>
>     ```sql
>     -- q6
>     SELECT COUNT(*)
>     FROM Villes
>     WHERE nom LIKE '%e' OR nom LIKE '%s'
>     ```

### 5. Opérations ensemblistes

Il est possible de combiner les ensembles de lignes renvoyés par plusieurs requêtes en utilisant les opérateurs  `UNION`, `INTERSECT` ou `EXCEPT`.

> 1. Tester :
>
>     ```sql
>     -- 5. opérations ensemblistes
>     
>     -- q1
>     SELECT prenom, nom
>     FROM Peintres
>     WHERE annee_naissance >= 1800
>         INTERSECT
>     SELECT prenom, nom
>     FROM Peintres
>     WHERE annee_mort < 1900
>     ```
>
> 3. Tester :
>
>     ```sql
>    -- q2
>    SELECT prenom
>    FROM Peintres
>        UNION
>    SELECT nom
>    FROM Peintres
>    ```
>
> 4. À l'aide du mot-clef `EXCEPT`, afficher les années de mort de peintres qui ne sont pas aussi des années de naissance d'autres peintres.
>
>     *Vous devriez en trouver 22.*

Deux remarques sur les opérateurs ensemblistes :

* Il n'y a pas de doublons dans le résultat.
* Les attributs du premier `SELECT` doivent avoir le même type que ceux du second.

### 6. Requêtes imbriquées

Une sous-requête est une requête imbriquée dans une autre requête SQL, on la place entre parenthèses. On peut notamment avoir une sous-requête dans le `FROM` ou dans le `WHERE`.

> 1. Tester cette requête, qui compte le nombre de couples de noms et prénoms attribués à des peintres :
>
>     ```sql
>     -- 6. requêtes imbriquées
>     
>     -- q1
>     SELECT COUNT(*)
>     FROM (	SELECT DISTINCT nom, prenom
>     		FROM Peintres )
>     ```
>
> 2. Tester cette requête, qui permet de trouver les nom et prénom du peintre qui est né en premier :
>
>     ```sql
>     -- q2
>     SELECT prenom, nom
>     FROM Peintres
>     WHERE annee_naissance = (	SELECT MIN(annee_naissance)
>     							FROM Peintres )
>     ```
>
> 3. Que fait cette requête ?
>
>     ```sql
>     -- q3
>     SELECT *
>     FROM Peintres
>     WHERE annee_mort - annee_naissance >= (SELECT AVG(annee_mort - annee_naissance)
>                                            FROM Peintres )
>     ```
>
> 4. À l'aide d'une requête imbriquée, donner le(s) nom(s) des musées dont le nom est de taille maximale.

### 7. Regroupements

Un groupe est un sous-ensemble de lignes d’une table avec la même valeur pour un ou plusieurs attributs donnés.

On peut regrouper toutes les lignes ayant la même valeur pour un attribut à l'aide de `GROUP BY`. Cela permet de faire des traitements sur un groupe entier plutôt que sur chaque ligne individuellement.

> 1. Tester :
>
>     ```sql
>     -- 7. regroupements
>
>     -- q1
>     SELECT id_ville
>     FROM Musees
>     ```
>
>     Puis tester :
>
>     ```sql
>     SELECT id_ville
>     FROM Musees
>     GROUP BY id_ville
>     ```
>
>     Tous les musées se situant dans la même ville sont regroupés et donc sont traités comme une seule ligne dans le résultat.
>
> 2. On peut alors compter le nombre de musée dans chaque ville ainsi :
>
>     ```sql
>     -- q2
>     SELECT id_ville, COUNT(*) AS 'nombre de musées dans cette ville'
>     FROM Musees
>     GROUP BY id_ville
>     ```
>
>     Plutôt que d'être effectuée sur toute la table, la fonction d'agrégation `COUNT` est appelée sur chaque groupe. Ainsi on ne compte pas tous les musées existants ici, on compte en fait pour chaque groupe de musée (ceux ayant la même ville) le nombre de musées du groupe.
>
> 3. Proposer une requête pour compter le nombre de villes de chaque pays.
>
> 4. On peut bien sûr combiner tout ce qu'on a vu jusque maintenant pour faire des requêtes plus élaborées. Expliquer ce que fait la requête suivante :
>
>     ```sql
>     -- q4
>     SELECT AVG(nb_oeuvres)
>     FROM (  SELECT id_peintre, COUNT(*) AS 'nb_oeuvres'
>             FROM Oeuvres
>             GROUP BY id_peintre )
>     ```
>     
> 5. Quel est le nombre maximal d’œuvres ayant le même nom ?

Il est possible de faire des groupes sur la base de plusieurs attributs en commun également.

> 6. Tester :
>
>     ```sql
>     -- q6
>     SELECT id_peintre, id_musee, COUNT(*) AS 'nombre d''oeuvres du même peintre exposées dans le même musée'
>     FROM Oeuvres
>     WHERE id_musee IS NOT NULL
>     GROUP BY id_peintre, id_musee
>     ```

Il est possible ensuite de ne sélectionner que des groupes respectant certains critères.

Par exemple, reprenons la requête ci-dessus qui compte le nombre de musée dans chaque ville. On souhaite ne garder que les villes qui ont au moins 2 musées. Il n'est pas possible de mettre cette condition dans le `WHERE` car le `WHERE` se place avant le `GROUP BY`. Or on n'obtient le compte des musées qu'après le `GROUP BY`. Il nous faut donc un autre mot-clef, `HAVING` qui lui se place après le `GROUP BY` et qui fait un deuxième filtrage des résultats, mais agissant cette fois sur les groupes.

> 7. Tester :
>
>     ```sql
>     -- q7
>     SELECT id_ville AS 'villes ayant au moins 2 musées', COUNT(*) AS 'nombre de musées dans cette ville'
>     FROM Musees
>     GROUP BY id_ville
>     HAVING COUNT(*) >= 2
>     ```

On peut bien évidemment écrire des requêtes ayant à la fois un `WHERE` et un `HAVING` :

* le `WHERE` filtre les *lignes* selon certains critères *AVANT* que les groupes soient faits ;
* le `HAVING` filtre les *groupes* selon certains critères *APRÈS* que les groupes soient faits.

> 8. Expliquer le résultat de la requête suivante :
>
>     ```sql
>     -- q8
>     SELECT id_ville AS '?????', COUNT(*) AS 'nombre de musées dans cette ville'
>     FROM Musees
>     WHERE nom <> 'Musée du Louvre'
>     GROUP BY id_ville
>     HAVING COUNT(*) >= 2
>     ```
>
> 9. Modifier la requête suivante pour ne conserver que les peintres qui exposent au moins 5 œuvres dans le même musée :
>
>     ```sql
>     -- q9
>     SELECT id_peintre, id_musee, COUNT(*) AS 'nombre d''oeuvres du même peintre exposées dans le même musée'
>     FROM Oeuvres
>     WHERE id_musee IS NOT NULL
>     GROUP BY id_peintre, id_musee
>     ```
>
> 10. * Écrire une requête qui affiche les différentes années de morts et le nombre de peintres morts cette année là.
>      * Modifier la requête pour n'afficher que les années de mort pour lesquelles la moyenne des années de naissance du groupe se situe entre 1400 et 1600.
>      * Modifier la requête pour ne prendre en compte que les peintres possédant un nom commun.

### 8. Jointures

Il nous reste un dernier point à voir : comment relier plusieurs tables entre elles. En effet pour l'instant nos requêtes n'agissaient que sur une table à la fois.

On peut souhaiter afficher par exemple à la fois le nom des musées et le noms des villes où ils se trouvent, or ces deux informations se trouvent dans deux tables différentes.

Un premier moyen de relier deux tables est de faire un produit cartésien : chaque ligne de la première table est juxtaposée à chaque ligne de la seconde table.

> 1. Tester :
>
>     ```sql
>     -- 8. jointures
>                                 
>     -- q1
>     SELECT *
>     FROM Musees, Villes
>     ```
>
>     Expliquer pourquoi ça ne convient pas si on souhaite trouver la ville où se trouve un musée.

Les produits cartésiens ne sont pas très utiles en pratique car ils ne permettent pas de relier deux tables de manière cohérente avec les données.

Si on souhaite relier deux tables, on possède déjà un attribut (ou groupe d'attributs) qui nous indique comment faire le lien : c'est la clé étrangère.

Pour nos tables *Musees* et *Villes*, la clé étrangère est l'attribut *id_ville* de la table *Musees* qui fait référence à l'attribut *id* de la table *Villes*. On relie donc nos deux tables selon cette clé étrangère.

Pour cela on utilise une **jointure**, qui s'écrit ainsi :

```sql
SELECT ...
FROM Table1
JOIN Table2 ON Table1.attribut_de_la_clé_étrangère = Table2.attribut_référencé
WHERE ...
```

> 2. Tester, et vérifier que cette fois-ci les informations correspondent bien :
>
>     ```sql
>     -- q2
>     SELECT *
>     FROM Musees
>     JOIN Villes ON Musees.id_ville = Villes.id
>     ```
>

Comme on a 2 tables différentes dans la requête, on doit préciser d'où vient l'attribut utilisé : on est obligés par exemple d'écrire *Villes.id* et non juste *id* car les deux tables *Musees* et *Villes* possèdent un attribut de ce nom et il y aurait ambiguïté. C'est là alors que va nous être utile le renommage !

> 3. Tester :
>
>     ```sql
>     -- q3
>     SELECT M.id, M.nom, V.id, V.nom
>     FROM Musees AS M
>     JOIN Villes AS V ON M.id_ville = V.id
>     ```
>
> 4. Que fait la requête suivante ?
>
>     ```sql
>    -- q4
>     SELECT UPPER(O.nom) || ' est exposé au ' || LOWER(M.nom) AS 'quelques renseignements'
>     FROM Oeuvres AS O
>     JOIN Musees AS M ON O.id_musee = M.id
>     WHERE LENGTH(O.nom) <= 15 AND M.nom LIKE 'Musée%'
>    ```

On peut bien entendu relier autant de tables que nécessaire : il suffit à chaque fois d'utiliser les clés étrangères.

> 5. Tester :
>
>     ```sql
>     -- q5
>     SELECT M.nom, V.nom, P.nom
>     FROM Musees AS M
>     JOIN Villes AS V ON M.id_ville = V.id
>     JOIN Pays AS P ON V.id_pays = P.id
>     ```

Si on souhaite relier deux tables qui n'ont pas de clé étrangère directe, on peut passer par d'autres tables intermédiaires. Par exemple si je souhaite relier *Musees* à *Pays* qui n'ont pas de lien direct il me suffit d'utiliser la clé étrangère de *Musees* à *Villes* puis celle de *Villes* à *Pays*.

> 6. Afficher pour chaque œuvre son nom et le nom du pays qui expose cet œuvre.
>
> 8. Que fait la requête suivante ?
>
>     ```sql
>     -- q7
>     SELECT O.nom, P1.nom AS 'pays d''origine du peintre', P2.nom AS 'pays d''exposition de l''oeuvre'
>     FROM Oeuvres AS O
>     JOIN Peintres AS P ON O.id_peintre = P.id
>     JOIN Pays AS P1 ON P.id_pays = P1.id
>     JOIN Musees AS M ON O.id_musee = M.id
>     JOIN Villes AS V ON M.id_ville = V.id
>     JOIN Pays AS P2 ON V.id_pays = P2.id
>     ```
>
>     On appelle cela une *auto-jointure* (on joint plusieurs fois la même table, ici *Pays*).
>     
> 8. Modifier la requête précédente pour ne conserver que les œuvres dont le pays d'origine du peintre est différent de celui d'exposition de l'œuvre.

Lorsque vous avez écrit la requête pour afficher pour chaque œuvre son nom et le nom du pays qui expose cet œuvre, certaines œuvres n'apparaissaient pas dans le résultat. En effet quand une œuvre n'est exposée nulle part, l'attribut vaut `NULL` et donc le lien n'est pas fait avec les autres tables.

On peut souhaiter conserver quand même dans le résultat les lignes pour lesquelles on n'arrive pas à faire de lien :

* un `LEFT JOIN` conserve les lignes pour lesquelles il y avait un `NULL` dans la table à gauche de la jointure (donc si on a `.... Table1 ... LEFT JOIN Table2 ON ....` ce sont les lignes avec des `NULL` de `Table1` qui sont conservées)
* (hors programme : il existe aussi `RIGHT JOIN` et `FULL JOIN`)

> 9. Tester :
>
>     ```sql
>     -- q9
>     SELECT O.nom AS 'nom de l''oeuvre', Pays.nom AS 'pays d''exposition de l''oeuvre'
>     FROM Oeuvres AS O
>     LEFT JOIN Musees AS M ON O.id_musee = M.id
>     LEFT JOIN Villes AS V ON M.id_ville = V.id
>     LEFT JOIN Pays ON V.id_pays = Pays.id
>     ```
>
>     Vérifier que cette fois-ci les deux œuvres qui sont exposées nulle part apparaissent bien.
>
> 10. Proposer une requête pour afficher, pour chaque ville, son nom et celui du pays auquel elle appartient. Les pays sans aucune ville devront apparaître également.
>

Vous connaissez maintenant tous les mots-clefs permettant d'écrire des requêtes SQL plus ou moins complexes :

```sql
SELECT SUM (nb_peintres_originaires_de_ce_pays) AS 'nombre total de peintres originaires de pays dont le nom termine par un "e" et ayant au moins 3 peintres'
FROM (	SELECT Pays.id, COUNT(*) AS 'nb_peintres_originaires_de_ce_pays'
		FROM Peintres AS P
		JOIN Pays ON P.id_pays = Pays.id
		WHERE Pays.nom LIKE '%e'
		GROUP BY Pays.id
		HAVING COUNT(*) >= 3 )
```

## III. Exercices

Vous connaissez maintenant tous les mots-clefs SQL permettant d'écrire des requêtes : `SELECT, DISTINCT, AS, FROM, WHERE, AND, OR, NOT, IS (NOT) NULL, LIKE, ORDER BY, LIMIT, OFFSET, UNION, INTERSECT, EXCEPT, MIN, MAX, SUM, AVG, COUNT, GROUP BY, HAVING, JOIN, LEFT JOIN`.

Une requête s'organise alors ainsi :

![](img/memento_sql.png)

À vous maintenant de trouver les bonnes requêtes permettant de répondre aux questions suivantes !

*Vous pouvez toujours vérifier vos réponses en parcourant les données à la main.*

Même si ce n'est pas précisé, on attend de vous **un renommage systématique des colonnes du résultat** si c'est nécessaire. C'est un peu comme donner des bons noms à vos variables en C ou en OCaml : ça facilite la lecture de votre requête.

> **Requêtes sans jointures**
>
> 1. Quels sont, sans doublons, les noms des musées ?
> 2. Quelles sont les œuvres qui ne sont pas exposées ?
> 3. Quels sont les deux derniers chiffres des années de naissance des peintres ?
> 4. Quels sont les huit derniers peintres morts ?
> 5. Quel est le nom commun des peintres qui en ont un ?
> 6. Quels sont, suivant l’ordre lexicographique croissant de leur nom, les troisième, quatrième et cinquième pays ?
> 7. Quel est le nom complet (prénom et nom en une seule chaîne) des peintres dont le prénom contient un 'a', le nom ne commence pas par 'Ve' et dont l’avant dernière lettre est un 'i' ?
> 8. Combien y a-t-il de peintres ayant vécu à partir du XVIème siècle dont le nom contient la particule *di* ou *van* ?
> 9. Donner de deux manières différentes (une avec une fonction d'agrégation, l'autre avec un formatage du résultat) le peintre mort en dernier.
> 10. Quels sont prénoms et noms des peintres qui n’ont vécu qu’au XVIIIe siècle ou dont l’identifiant du pays est 1 (la France) en ayant vécu au moins 70 ans ?
> 11. Quels sont les prénoms, noms et les durées de vie des peintres qui sont morts avant leur 40ème anniversaire ?
> 12. Quelles sont les années qui ont vu à la fois la naissance et la mort d’un peintre ?
> 13. Donner l’ensemble des noms qui sont des noms de famille ou des noms communs attribués à au moins un peintre.
> 14. Quels sont les identifiants des pays pour lesquels aucun peintre originaire de ce pays n’est né après 1800 ?
> 15. Y a-t-il des peintres ayant le même prénom et étant originaire du même pays ?
> 16. Quel est le nom et l’année de naissance des peintres nés le plus récemment ?
> 17. Combien de peintres sont nés après l’année moyenne de naissance d’un peintre ?
> 18. Quel est le nombre moyen d’œuvres dans les musées qui comportent au moins deux œuvres ?
> 19. Quels sont les prénoms et noms de peintres, ainsi que les noms des pays dans lesquels un peintre a pu voyager, sachant que l’on ne prend pas en compte un pays dont le peintre est originaire ?

> **Requêtes avec jointures**
>
> 1. Quels sont les noms des peintres avec le nom de leurs œuvres ?
> 2. Quels sont les noms des œuvres exposées au Musée du Louvre ?
> 3. Quels sont, sans doublons, les identifiants et noms des musées qui exposent au moins une œuvre du peintre de nom commun Le Titien ?
> 4. Combien d’œuvres sont exposées au Musée d'Orsay ?
> 5. Donner, pour chaque peintre ayant au moins une œuvre, son prénom, son nom et son nombre d’œuvres.
> 6. Donner le nom des musées qui exposent au moins deux tableaux dont le titre commence par L, avec ce nombre de tableaux.
> 7. Donner, pour chaque musée dont le nom commence par ’G’ et qui expose au moins 2 œuvres de peintres nés après 1480, son nom et le nombre de peintres nés après 1480 qui y sont exposés.
> 8. Donner, pour chaque pays, son nom, le nombre de peintres qui en sont originaires et la durée moyenne de vie d’un peintre originaire de ce pays.
> 9. Quels sont, sans doublons, les noms des peintres et les noms des musées situés en France qui exposent au moins une de leurs œuvres ?
> 10. Quel est le nombre de lettres total des noms des musées qui ne sont pas en France ?
> 11. Donner les noms des œuvres et de leurs peintres triés par date de naissance des peintres dans l’ordre décroissant puis, pour une même date de naissance du peintre, par ordre lexicographique croissant du nom de l’œuvre.

> **Encore plus d'entraînement**
>
> 1. Donner, pour chaque peintre, son identifiant et son nombre d’œuvres. Les peintres ayant aucune œuvre doivent également apparaître dans le résultat.
>
> 2. Quel est le nom d’un musée qui expose le moins d’œuvres ? On demande un seul musée parmi ceux qui exposent le moins d’œuvres.
>
> 3. Donner les identifiants des peintres dont aucune œuvre n’est exposée.
>
> 4. Donner les noms, sans doublons, des pays dont au moins un peintre est originaire et qui contiennent au moins un musée.
>
> 5. Y a-t-il des pays qui contiennent un musée mais dont aucun peintre n’est originaire ?
>
> 6. Écrire une requête qui aboutit sur une table d’attributs « id, nom », où nom est le nom d’usage d’un peintre s’il en a un et le nom de famille sinon.
>
> 7. Donner, sans doublons, les noms de tableaux qui sont ceux d’au moins deux œuvres différentes.
>
> 8. Quels sont les identifiants, prénoms et noms des peintres originaires du même pays que le peintre connu sous le nom commun Sandro Botticelli ?
>
> 9. Donner, sans doublons, le nom des peintres qui sont exposés dans un musée dans lequel il y a au moins un autre peintre de même nationalité que ce peintre qui est exposé.
>
> 10. Donner le nom des peintres qui ont peint au moins une œuvre exposée dans un pays dans lequel est exposé dans une autre ville de ce pays au moins une œuvre d’un peintre qui est né ou mort strictement plus tôt.
>
> 11. Quels sont les noms des peintres exposés dans tous les musées parisiens ?
>
>     *Indication : compter le nombre de musées parisiens exposant chaque peintre...*

## Pour aller plus loin

> Inventez vos propres questions concernant cette base, et demandez à votre voisin d'y répondre avec une requête SQL !

![](https://imgs.xkcd.com/comics/exploits_of_a_mom.png)

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Source des images : *production personnelle*

Inspirations : N. Pecheux
