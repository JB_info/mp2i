# TP : Graphes en OCaml

Nous allons ici découvrir, en OCaml, la structure de « graphe », orienté et non orienté.

***Dans l'intégralité de ce TP, l'utilisation de références sur les listes est interdite !***

## I. Graphes non orientés

Un graphe non orienté fini $`G = (S,A)`$ est défini par :

* l'ensemble fini non vide $`S = \{s_1, s_2, ..., s_n\}`$ dont les éléments sont appelés **sommets** ;
* l'ensemble fini $`A \subset S^2`$ composé de paires $`\{s_i,s_j\}`$ appelées **arêtes**.

Voici un exemple de graphe, qu'on appellera $`G_{no}`$ :

![](img/graphe_non_oriente.png)

On note $`|S|`$ le nombre de sommets du graphe et $`|A|`$ le nombre d'arêtes. On exprimera souvent nos complexités en fonction de ces deux valeurs.

> 1. Pour le graphe  $`G_{no}`$ représenté ci-dessus, que vaut $`|S| \text{ ? } |A|`$ ?

Il y a deux manières de représenter un graphe : par sa matrice d'adjacence ou par sa liste d'adjacence.

Une **matrice d'adjacence** est une matrice carrée de taille $`|S| \times |S|`$ définie par : $`\begin{cases} M_{i,j} = 1 \Leftrightarrow \{i,j\} \in A \\ M_{i,j} = 0 \Leftrightarrow \{i,j\} \notin A \end{cases}`$

Voici un exemple de graphe non orienté accompagné de sa matrice :

![](img/matrice_dadjacence.png)

> 2. Donnez la matrice d'adjacence du graphe $`G_{no}`$.

En OCaml, on utilisera le type suivant :

```ocaml
type matrice_adjacence = int array array
```

> 3. Écrire une fonction `nb_sommets_aretes : matrice_adjacence -> int * int` qui renvoie le nombre de sommets et le nombre d'arêtes d'un graphe non orienté représenté par sa matrice d'adjacence.
>
>     Quelle est sa complexité ?
>     
>     Pour tester, voici la matrice d'adjacence du graphe $`G_{no}`$ :
>     
>     ```ocaml
>     let mat_gno = [|[|0; 1; 0; 0; 1; 1; 0; 0; 0|];
>                     [|1; 0; 0; 1; 0; 0; 0; 0; 0|];
>                     [|0; 0; 0; 0; 1; 1; 1; 0; 0|];
>                     [|0; 1; 0; 0; 0; 1; 0; 0; 0|];
>                     [|1; 0; 1; 0; 0; 1; 0; 0; 1|];
>                     [|1; 0; 1; 1; 1; 0; 1; 0; 0|];
>                     [|0; 0; 1; 0; 0; 1; 0; 1; 0|];
>                     [|0; 0; 0; 0; 0; 0; 1; 0; 0|];
>                     [|0; 0; 0; 0; 1; 0; 0; 0; 0|]|]
>     ```

La représentation d'un graphe par sa **liste d'adjacence** consiste à stocker, pour chaque sommet, la liste des sommets auxquels il est relié.

Voici un exemple :

![](img/liste_dadjacence.png)

> 4. Donnez la liste d'adjacence du graphe $`G_{no}`$.

En OCaml, on utilisera le type suivant :

```ocaml
type liste_adjacence = int list array
```

> 5. Écrire une fonction `nb_sommets_aretes_lst : liste_adjacence -> int * int` qui renvoie le nombre de sommets et le nombre d'arêtes d'un graphe non orienté représenté par sa liste d'adjacence.
>
>     Quelle est sa complexité ? Comparez avec la version sur la matrice d'adjacence.
>
>     Pour tester :
>     
>     ```ocaml
>     let lst_gno = [| [1;4;5]; [0;3]; [4;5;6]; [1;5]; [0;2;5;8]; [0;2;3;4;6]; [2;5;7]; [6]; [4] |]
>     ```

En fonction du problème étudié, on privilégiera souvent une représentation ou l'autre.

> 6. Écrire une fonction qui prend en paramètre la liste d'adjacence d'un graphe et renvoie la matrice d'adjacence correspondante. On rappelle que l'utilisation de références sur les listes est interdite.
> 7. Même question dans l'autre sens. Les listes devront contenir les sommets dans l'ordre croissant.

> Pour chaque fonction suivante, vous écrirez deux versions, utilisant respectivement la représentation du graphe sous forme de matrice d'adjacence et sous forme de liste d'adjacence, et vous comparerez les complexités.
>
> 8. Deux sommets sont dits **voisins** s'il existe une arête les reliant. Écrire une fonction qui prend en paramètres un graphe et deux sommets et détermine s'ils sont voisins.
> 9. Une **boucle** est une arête reliant un sommet à lui-même. Écrire une fonction qui prend en paramètre un graphe et détermine s'il possède une boucle.
> 10. Le **degré** d'un sommet est son nombre de voisins. Écrire une fonction qui prend en paramètre un graphe et un sommet et renvoie son degré. En déduire une fonction qui renvoie le sommet de degré maximal d'un graphe et son degré.

## II. Graphes orientés

Un graphe orienté fini $`G = (S,A)`$ est défini par :

* l'ensemble fini non vide $`S = \{s_1, s_2, ..., s_n\}`$ dont les éléments sont appelés sommets ;
* l'ensemble fini $`A \subset S^2`$ composé de *couples* $`(s_i,s_j)`$ appelées **arcs**.

Voici un exemple de graphe orienté, qu'on appellera $`G_o`$ :

![](img/graphe_oriente.png)

Intuitivement, un arc $`(s_i, s_j)`$ permet de passer du sommet $`s_i`$ au sommet  $`s_j`$ mais pas de  $`s_j`$ à  $`s_i`$. Les représentations par matrice d'adjacence et par liste d'adjacence suivent un principe similaire à celles des graphes non orientés.

> 1. Donnez la matrice d'adjacence et la liste d'adjacence du graphe $`G_o`$.
> 2. Vos fonctions pour convertir une matrice en liste et inversement sont-elles applicables aux graphes orientés ? Si non, modifiez les.
> 3. Comment déterminer à partir d'une matrice d'adjacence si le graphe est orienté ou non ? Écrire une fonction qui prend en paramètre la matrice d'adjacence d'un graphe et détermine s'il est orienté ou non.
> 4. Même question pour une liste d'adjacence.
>
> Pour tester, voici les deux représentations de $`G_o`$ :
>
> ```ocaml
> let mat_go = [|[|0; 1; 0; 0; 0; 0; 0; 0; 0|];
>                 [|1; 0; 0; 1; 0; 0; 0; 0; 0|];
>                 [|0; 0; 0; 0; 1; 1; 0; 0; 0|];
>                 [|0; 0; 0; 0; 0; 0; 0; 0; 0|];
>                 [|1; 0; 0; 0; 0; 0; 0; 0; 1|];
>                 [|1; 0; 1; 1; 1; 0; 1; 0; 0|];
>                 [|0; 0; 1; 0; 0; 0; 0; 0; 0|];
>                 [|0; 0; 0; 0; 0; 0; 1; 0; 0|];
>                 [|0; 0; 0; 0; 0; 0; 0; 0; 0|]|]
> 
> let lst_go = [| [1]; [0;3]; [4;5]; []; [0;8]; [0;2;3;4;6]; [2]; [6]; [] |]
> ```

La majorité du vocabulaire sur les graphes non orientés ne peut pas s'appliquer aux graphes orientés.

> Pour chaque fonction suivante, vous écrirez deux versions, utilisant respectivement la représentation du graphe sous forme de matrice d'adjacence et sous forme de liste d'adjacence, et vous comparerez les complexités.
>
> On rappelle que l'utilisation de références sur les listes est interdite.
>
> 5. Écrire une fonction qui renvoie le nombre de sommets et le nombre d'arcs d'un graphe.
> 6. S'il y a un arc $`(s_i, s_j)`$, on dit que $`s_j`$ est un **successeur** de $`s_i`$ et que $`s_i`$ est un **prédécesseur** de $`s_j`$. Écrire une fonction qui prend un paramètres un graphes et deux sommets et détermine si le premier est un successeur du second. Même question pour un prédécesseur.
> 7. Écrire une fonction qui prend en paramètre un graphe et renvoie un tableau contenant, pour chaque sommet, la liste de ses prédécesseurs.
> 8. Le **degré sortant** d'un sommet, noté $`d_+(s)`$, est son nombre de successeurs et le **degré entrant**, noté $`d_-`$​, est son nombre de prédécesseurs. Écrire une fonction qui prend en paramètre un graphe et un sommet et renvoie son degré sortant et son degré entrant.

## III. Parcours de graphes

Parcourir un graphe est une tâche assez similaire au parcours d’un arbre, avec quelques différences importantes :

* il n’y a pas de « racine » : en règle générale, on parcourt à partir d’un sommet `x` et l’on ne parcourra bien sûr que les sommets accessibles à partir de `x` ;
* le plus important : a priori, il y a des cycles (chemins ayant le même sommet de départ et d'arrivée), et il ne faut pas tourner en rond ! D’une manière ou d’une autre, il faut donc se souvenir des sommets que l’on a déjà visités.

Voici l'algorithme pour un **parcours en profondeur** du graphe `G` à partir du sommet `v` :

![](img/graphe_parcours_profondeur.png)

> 1. Déroulez le parcours en profondeur à la main sur les graphes $`G_{no}`$ et $`G_o`$ à partir du sommet 0.
> 2. On devra tester l'appartenance à la structure `vus` de nombreuses fois, il est important que cette opération soit efficace. Quelle structure de données vous semble appropriée ?
> 3. Implémentez un parcours en profondeur de graphe. Le traitement consistera simplement à afficher les sommets parcourus. Vérifiez vos réponses à la question précédente.

Voici l'algorithme pour un **parcours en largeur** du graphe `G` à partir du sommet `v` :

![](img/graphe_parcours_largeur.png)

> 4. Déroulez le parcours en largeur à la main sur les graphes $`G_{no}`$ et $`G_o`$​ à partir du sommet 0.
> 5. Implémentez un parcours en largeur de graphe. Le traitement consistera simplement à afficher les sommets parcourus. Vérifiez vos réponses à la question précédente.

## IV. Chemin dans un graphe

Pour les fonctions de cette partie, vous écrirez deux versions, utilisant respectivement la représentation du graphe sous forme de matrice d'adjacence et sous forme de liste d'adjacence, et vous comparerez les complexités.

Vous vous assurerez également que vos fonctions peuvent s'appliquer à des graphes aussi bien orientés que non orientés.

Un **chemin** de longueur `p` dans un graphe est une suite de `p + 1` sommets $`s_0, s_1, ..., s_p \text { telle que }\forall i\in [\![0, p[\![, \{s_i, s_{i+1}\} \in A`$.

> 1. Écrire une fonction qui prend en paramètre un graphe et une liste de sommets et détermine si le chemin représenté par cette liste existe.

Un chemin est dit **élémentaire** s'il ne passe pas deux fois par le même sommet, et **simple** s'il ne passe pas deux fois par la même arête / le même arc.

> 2. Écrire une fonction qui prend en paramètre un chemin de longueur `p` et détermine si ce chemin est élémentaire en $`\mathcal O(p)`$.
> 3. Écrire une fonction qui prend en paramètre un chemin et détermine s'il est simple. On essaiera d'être d'efficace.

Dans certains cas, on peut avoir besoin de **pondérer** un graphe, c'est-à-dire ajouter une information, un **poids**, à chaque arête/arc. Pensez par exemple à un graphe dont les sommets représentent des villes, et dont les arêtes/arcs sont pondéré(e)s par les distances entre ces villes.

Voici un exemple de graphe non orienté pondéré :

![](img/graphe_pondere.png)

On peut bien entendu pondérer un graphe orienté également.

Pour représenter un graphe pondéré, on doit modifier les listes d'adjacence : on stockera dans les listes des couples (sommet, poids). Pour les matrices d'adjacence, on remplace les "1" par les poids des arêtes / arcs, et les "0" par $`+\infty`$.

> 4. Donnez la matrice et la liste d'adjacence du graphe pondéré ci-dessus.
> 11. Pourquoi doit-on utiliser $`+\infty`$ et non 0 dans les matrices d'adjacence ?
> 12. Définissez en OCaml un type pour représenter les listes d'adjacence de graphes pondérés.
> 13. Définissez un type somme pour représenter soit $`+\infty`$ soit un poids, puis un type pour représenter les matrices d'adjacence des graphes pondérés.

Dans un graphe pondéré, le poids d'un chemin est défini comme la somme des poids des arêtes / arcs qui le compose.

> 8. Écrire une fonction qui prend en paramètre un graphe et un chemin donné sous la forme d'une liste de sommets et renvoie le poids de ce chemin, ou $`+\infty`$ s'il s'agit d'un chemin invalide.

## Pour aller plus loin

> On peut implémenter un parcours en profondeur impératif à l'aide d'une pile. Essayez d'adapter vos algorithmes afin de réaliser ce parcours.


---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Source des images : *production personnelle*, Q. Fortier (matrice d'adjacence), J.B. Bianquis (parcours)
