# TP : Logique propositionnelle

**Ce TP est à faire en OCaml.**

La première partie s'intéresse aux expressions booléennes. La seconde partie implémente quelques fonctions classiques sur l'ensemble des formules propositionnelles. La troisième partie s'intéresse à la sémantique des formules propositionnelles. La dernière partie implémente l'algorithme de Quine afin de résoudre le problème SAT.

## I. Expressions booléennes et arbres

On appelle *expression booléenne* une formule propositionnelle ne contenant aucune variable propositionnelle.

En OCaml, on choisit ici de représenter les expressions booléennes par un arbre avec le type suivant :

```ocaml
type expr_bool = Noeud of string * expr_bool list
```

Les nœuds de l'arbre seront étiquetés par une des chaînes de caractères suivantes :

* "non" (nœud d'arité 1) ;
* "et" (nœud d'arité 2) ;
* "ou" (nœud d'arité 2) ;
* "faux" (nœud d'arité 0) ;
* "vrai" (nœud d'arité 0).

> 1. L'arbre binaire d'une expression booléenne est-il strict ou non strict ?
>
> 2. Représentez graphiquement l'arbre implémenté par :
>
>     ```ocaml
>     let expr_test = Noeud("et", [ Noeud("ou", [  Noeud("vrai",[]);
>                                                  Noeud("faux",[]) ]);
>                                   Noeud("non", [ Noeud("et", [ Noeud("faux",[]);
>                                                                Noeud("ou",[ Noeud("non",[Noeud("vrai",[])]);
>                                                                             Noeud("faux",[])])])])])
>     ```
>
> 3. Écrivez une fonction `est_correct : expr_bool -> bool` qui détermine si l'arbre passé en paramètre a la syntaxe correcte pour représenter une expression booléenne.

Les connecteurs "et", "ou" et "non" sont définis par leurs tables de vérité, qui prennent leurs valeurs dans `{F, V}`.

> 4. Déterminez la valeur de vérité de l'expression booléenne `expr_test` ci-dessus.
>
> 5. Écrivez une fonction `evalue_v1 : expr_bool -> bool` qui renvoie la valeur de vérité (`false` pour F et `true` pour V) d'une expression booléenne.
>
>     Vérifiez à l'aide de cette fonction votre réponse à la question précédente.

Les connecteurs "ou" et "et" étant associatifs, les expressions booléennes suivantes sont équivalentes :  $`b_1\land b_2 \land b_3 \;\;\;\;b_1 \land (b_2 \land b_3) \;\;\;\; (b_1 \land b_2) \land b_3`$. Ainsi, il n'est pas nécessaire d'imposer que les nœuds étiquetés par "et" et "ou" soient d'arité 2.

> 6. Écrivez une fonction `evalue_v2 : expr_bool -> bool` similaire à la question précédente, mais pouvant prendre en paramètre des expressions booléennes dont l'arbre contient des nœuds "et" et "ou" d'arité supérieure à 2.
>
>     *Indication : pensez à utiliser les fonctions du module `List` !*

En réalité, définir l'arbre comme nous l'avons fait dans cette partie n'est pas très pratique, étant donné qu'il n'y a qu'un nombre limité de chaînes de caractères pour lesquelles un nœud est défini.

## II. Fonctions sur l'ensemble des formules propositionnelles

On considère un ensemble dont les éléments sont appelés **variables propositionnelles**, qu'on notera $`\mathcal V = \{x_0,x_1,...\}`$. On définit l'ensemble des **formules propositionnelles** $`\mathcal P`$ par induction :

* $`\mathcal V \subset \mathcal P`$
* $`\top \in \mathcal P`$ (lire "top")
* $`\bot \in \mathcal P`$ (lire "bottom")
* $`p\in \mathcal P \Rightarrow \lnot p \in \mathcal P`$ (lire "non")
* $`p,q\in \mathcal P \Rightarrow p\land q \in \mathcal P`$ (lire "et")
* $`p,q\in \mathcal P \Rightarrow p\lor q \in \mathcal P`$​ (lire "ou")

> 1. Quelles sont les assertions dans cette définition ? les règles d'inférence ?
> 2. Cette définition est-elle ambiguë ? Si oui, modifiez la légèrement.

Nous utiliserons le type suivant pour représenter une formule propositionnelle :

```ocaml
type formule =
	| Top
	| Bottom
	| Var of int
	| Non of formule
	| Ou of formule * formule
	| Et of formule * formule
```

Voici un exemple de formule qui pourra vous servir pour tester vos fonctions :

```ocaml
let ex = Non (Ou (Et (Var 0, Var 1), Et (Var 0, Var 2)))
```

> 3. Donnez la formule propositionnelle correspondant à la variable `ex`. Dessinez l'arbre syntaxique correspondant.
>
> 4. Quel type de parcours d'arbre faut-il effectuer pour récupérer la formule écrite avec la syntaxe usuelle ? Et pour récupérer la formule écrite en OCaml ?
>
> 5. Écrivez une fonction `string_of_formule : formule -> string` renvoyant la représentation usuelle d'une formule propositionnelle sous forme d'une chaîne de caractères. Attention au parenthésage.
>
>     ```ocaml
>     # string_of_formule ex ;;
>     - : string = "¬((x_0 ∧ x_1) ∨ (x_0 ∧ x_2))"
>     # string_of_formule (Ou (Et (Var 1, Bottom), Non (Var 4))) ;;
>     - : string = "((x_1 ∧ ⊥) ∨ ¬x_4)"
>     ```
>
> 7. Écrivez une fonction `nb_var_distinctes : formule -> int` qui renvoie le nombre de variables propositionnelles distinctes apparaissant dans une formule. *On essaiera d'être efficace.*

Nous allons définir quelques fonctions sur l'ensemble des formules propositionnelles.

> 7. Rappelez la définition inductive de la taille d'une formule propositionnelle. Déduisez-en une fonction récursive `taille : formule -> int`.
> 8. Rappelez la définition inductive de la hauteur d'une formule propositionnelle. Déduisez-en une fonction `hauteur : formule -> int`.
> 9. Écrivez une fonction `substitue : formule -> int -> formule -> formule` telle que `substitue p x q` renvoie $`P[Q/x]`$​.
> 10. Écrivez une fonction `ensemble_sous_formules : formule -> formule list` qui renvoie l'ensemble des sous-formules d'une formule propositionnelle. La liste renvoyée pourra éventuellement contenir des doublons.
> 11. Écrivez une fonction `sous_formule : formule -> formule -> bool` qui détermine si une formule est une sous-formule d'une autre.

## III. Sémantique des formules propositionnelles

On utilise dans cette partie le même type `formule` défini dans la partie précédente.

On définit une valuation comme un tableau de booléens :

```ocaml
type valuation = bool array
```

Si `v` est de type `valuation`, alors `v.(i)` donne la valeur de vérité associée à la variable propositionnelle `Var i`.

> 1. Écrivez une fonction `max_var : formule -> int` qui calcule l’indice maximal d’une variable propositionnelle utilisée dans une formule.
> 2. Écrivez une fonction `peut_etre_evaluee : formule -> valuation -> bool` qui indique si une formule peut être évaluée selon une valuation donnée.
> 3. Écrivez une fonction `satisfait : formule -> valuation -> bool` qui indique si une valuation satisfait une formule (satisfait = est un modèle de) .
> 4. Dressez la table de vérité de la formule correspondant à la variable `ex` définie dans la partie précédente. Pour chaque valuation existante, vérifiez ensuite votre résultat à l'aide la fonction `satisfait`. Combien y a-t-il de valuations à tester ?
> 5. Écrivez une fonction `consequence : formule -> formule -> valuation list -> bool` telle que `consequence p q l` détermine si $`P\vDash Q`$ avec `l` la liste des valuations existantes.
> 6. Écrivez une fonction `equivalence : formule -> formule -> valuation list -> bool` telle que `equivalence p q l` détermine si $`P\equiv Q`$​ avec `l` la liste des valuations existantes.
> 7. Montrez les lois de De Morgan à l'aide de la fonction précédente.

Il existe d'autres connecteurs logiques que les trois principaux faisant partie de la définition inductive donnée plus haut. Voici leurs tables de vérité :

![](img/connecteurs_logiques.png)

Il n'est cependant pas nécessaire de modifier le type `formule` pour les inclure car il est possible de les ré-écrire uniquement avec les connecteurs de négation, conjonction et disjonction.

Par exemple, $`P \rightarrow Q \equiv \lnot P\lor Q`$. Pour le montrer, on compare les tables de vérité des deux formules.

> 8. Donnez la table de vérité de $`\lnot P\lor Q`$. Justifiez alors l'équivalence avec l'opérateur d'implication $`\rightarrow`$​. En utilisant ce résultat, définissez une fonction `implique : formule -> formule -> formule` telle que `implique p q` renvoie la formule correspondant à $`P \rightarrow Q`$​.
> 9. Exprimez tous les connecteurs donnés dans le tableau ci-dessus en utilisant uniquement $`\land, \lor, \lnot`$. Définissez alors, pour chaque connecteur `conn`, une fonction OCaml qui prend en paramètres deux formules `p` et `q` et renvoie la formule correspondante à `p conn q`.

Si P est une formule, on peut définir les quantificateurs universel et existentiel ainsi :

* $`\forall x. P \equiv P[\top/x]\land P[\bot/x]`$
* $`\exists x. P \equiv P[\top/x]\lor P[\bot/x]`$

> 10. Écrivez deux fonctions `quantif_universel : int -> formule -> formule` et `quantif_existentiel : int -> formule -> formule`.
>
> 11. Modifiez le type `formule` afin d'y ajouter directement les deux quantificateurs ainsi que les connecteurs d'implication et d'équivalence. Les variables propositionnelles seront désormais de type `'a`. On appellera ce type `formule_complete`.
>
> 12. Définissez la formule propositionnelle complète correspondant à l'arbre suivant :
>
>     ![](img/arbre_logique.png)

Une occurrence d'une variable x dans une formule P est un nœud de l'arbre étiqueté par x. Une occurrence de x est dite libre si le chemin reliant l'occurrence à la racine ne contient pas de nœud $`\forall x`$ (ou plus précisément, de nœud $`\forall`$ dont le fils gauche est x) ni de nœud $`\exists x`$. Une occurrence de x est dite liée dans le cas contraire. Une variable est dite libre dans une formule P si elle a au moins une occurrence libre, liée si elle a au moins une occurrence liée.

> 13. Écrivez deux fonctions `est_libre : formule_complete -> int -> bool` et `est_liee : formule_complete -> int -> bool` qui indique si une variable est libre / liée dans une formule.
> 14. (Question facultative) Écrivez une fonction `portee : formule_complete -> bool * int -> formule_complete option` qui prend en paramètre une formule P et un quantificateur sous la forme d'un couple `(b, i)` où `b` vaut `true` pour désigner le quantificateur universel et `false` pour désigner le quantificateur existentiel, et `i` désigne une variable propositionnelle ; et renvoie la portée de ce quantificateur. S'il y en a plusieurs, on renverra celui situé le plus à gauche dans l'arbre de P. S'il n'apparaît pas dans la formule, on renverra `None`.

## IV. Problème SAT

On aura besoin pour cette partie des types `formule` et `valuation`, et des fonctions `max_var`, `substitue` et `satisfait` définies dans les parties précédentes. On s'intéresse ici au problème SAT, c'est-à-dire qu'on cherche à savoir si une formule propositionnelle est satisfiable.

On propose d'abord une approche par force brute, sur un principe similaire à celui des tables de vérité : on évalue la formule pour chacune des valuations possibles et on regarde si l'une d'elle donne V. Pour passer d'une valuation à une autre, on propose la fonction suivante :

```ocaml
exception Derniere_valuation
let incremente_valuation (v : valuation) : unit =
    let i = ref (Array.length v - 1) in
    while !i >= 0 && v.(!i) do
        v.(!i) <- false ;
        decr i
    done ;
    if !i < 0 then
        raise Derniere_valuation
    else
        v.(!i) <- true
```

La première valuation à tester est celle associant F à toutes les variables. Ensuite, on teste toutes les valuations existantes en appelant la fonction `incremente_valuation` jusqu'à ce la fonction lève l'exception `Derniere_valuation`.

> 1. Écrivez une fonction `satisfiable_brute : formule -> bool` qui détermine si une formule est satisfiable en essayant toutes les valuations possibles (jusqu'à les épuiser ou en trouver une convenable).
>

Il existe un algorithme plus efficace pour déterminer si une formule est satisfiable : l'**algorithme de Quine**, qui utilise une approche par *backtracking*. Pour appliquer l'algorithme de Quine, nous avons besoin de quelques règles qui permettent de simplifier une formule :

![](img/regles_quine.png)

> 2. Écrivez une fonction `elimine_constantes : formule -> formule` qui prend en entrée une formule `f` et utilise les règles ci-dessus afin de renvoyer une formule `f'` telle que $`f'\equiv f`$ et :
>     * soit `f'` est réduite à une constante
>     * soit `f'` ne contient aucune constante

Dans la suite, on notera `s(f)` la formule obtenue en appliquant `elimine_constantes` sur une formule `f`.

L'algorithme de Quine consiste, à partir d'une formule `f`, à :

* calculer `f' = s(f)`
* s'il reste au moins une variable propositionnelle
    * sélectionner arbitrairement une variable `x` restante dans la formule
    * tester récursivement si $`f'[\top/x]`$ est satisfiable
    * tester récursivement si $`f'[\bot/x]`$ est satisfiable
* sinon, conclure

> 3. Montrer par induction structurelle sur `f` que si `f` ne contient pas de variable alors `s(f)` est réduite à une constante. En déduire que l'algorithme de Quine termine.

Comme tout algorithme de backtracking, on peut dessiner l'arbre correspondant à l'algorithme de Quine.

Considérons par exemple la formule $`(a\lor b)\land (\lnot b \lor \lnot c)\land(\lnot a \lor c)\land(a\lor c)`$, qui est donc la racine de notre arbre.

On sélectionne la variable `a` arbitrairement, on va la substituer par $`\bot`$ puis poursuivre (sous-arbre gauche) et la substituer par $`\top`$ puis poursuivre également (sous-arbre droit).

Étudions par exemple la branche droite. Par substitution on obtient la formule $`(\top\lor b)\land (\lnot b \lor \lnot c)\land(\lnot \top \lor c)\land(\top\lor c)`$, puis en appliquant les règles de simplification pour enlever les constantes on trouve donc $`(\top\lor b)\land (\lnot b \lor \lnot c)\land(\lnot \top \lor c)\land(\top\lor c) \equiv \top\land (\lnot b \lor \lnot c)\land(\bot \lor c)\land\top \equiv (\lnot b \lor \lnot c)\land c`$.

On sélectionne à partir de là arbitrairement une variable restante, disons `b`. On substitue `b` par  $`\bot`$ puis on poursuit (sous-arbre gauche) et on la substitue par $`\top`$ puis on poursuit également (sous-arbre droit).

Suivons cette fois la branche gauche. Par substitution puis simplification on obtient $`(\lnot \bot \lor \lnot c)\land c \equiv (\top \lor \lnot c)\land c \equiv c`$.

On sélectionne la dernière variable restante, `c`, qu'on substitue dans la branche gauche par $`\bot`$ et dans a branche droite par $`\top`$.

Il ne reste plus aucune variable, les feuilles sont étiquetées par des constantes. Voici l'arbre correspondant au cheminement qu'on vient de faire :

![](img/quine.png)

> 4. Complétez les branches de l'arbre non détaillées plus haut.
> 5. À quelle condition sur l'arbre obtenu la formule est-elle satisfiable ? une tautologie ? une antilogie ?

***Il faut savoir implémenter l'algorithme de Quine.*** Sans plus de précision, cela signifie construire l'arbre de Quine associé à une formule.

On utilisera le type suivant :

```ocaml
type arbre_quine =
	| Feuille of bool
	| Branchement of int * arbre_quine * arbre_quine
```

> 6. Écrire une fonction `quine : formule -> arbre_quine` qui prend en paramètre une formule et renvoie l'arbre associé par l'algorithme de Quine.
> 7. En déduire une fonction `satisfiable_quine : formule -> bool` qui détermine si une formule est satisfiable en construisant l'arbre de décision de l'algorithme de Quine puis en regardant si une feuille vaut $`\top`$​.
> 8. Pour retrouver une valuation qui satisfait la formule, il suffit de retracer le chemin de la racine jusqu'à une feuille $`\top`$. Modifiez la fonction précédente pour renvoyer une valuation qui satisfait la formule propositionnelle. Le type sera `valuation_quine : formule -> valuation option` pour le cas où la formule n'est pas satisfiable.

Le problème **SAT** est un problème de décision qui consiste à se demander si une formule logique est satisfiable. SAT est un problème absolument central en informatique, parce que d’innombrables problèmes se réduisent naturellement à lui.

> 9. Voici le curieux règlement d’un club britannique :
>
>     * tout membre non écossais porte des chaussures rouges ;
>     * tout membre porte un kilt ou ne porte pas de chaussures rouges ;
>     * les membres mariés ne sortent pas le dimanche ;
>     * un membre sort le dimanche si et seulement s’il est écossais ;
>     * tout membre qui porte un kilt est écossais et marié ;
>     * tout membre écossais porte un kilt.
>
>     Ce règlement permet-il d’accueillir des membres ?
>
>     Répondez à cette question en déroulant l'algorithme de Quine à la main puis vérifiez votre réponse avec votre code OCaml.

## Pour aller plus loin

> 1. À l'aide d'un algorithme par force brute, écrire deux fonctions permettant respectivement de déterminer si une formule est une tautologie / une antilogie.
> 2. Avec l'algorithme de Quine, écrire une fonction qui détermine si une formule propositionnelle est une tautologie. Même question pour une antilogie.
> 3. Proposez une implémentation en C d'une formule propositionnelle, et implémentez alors l'algorithme de Quine.


---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Source des images : *production personnelle*
