# TP : Arbres en OCaml

La première partie de ce TP implémente quelques fonctions classiques sur les arbres (binaires et d'arité quelconque), et la seconde partie s'intéresse à la construction d'arbres.

Le TP est à faire en OCaml. L'utilisation de toutes les fonctions du module `List` vues en cours est autorisée (et même encouragée !).

## I. Fonctions sur les arbres

### 1. Arbres binaires

On considère un ensemble $\mathcal F$ (étiquettes des feuilles), et un ensemble $\mathcal I$ (étiquettes des nœuds internes). On définit l'ensemble $\mathcal A(\mathcal I, \mathcal F)$ des arbres binaires stricts par induction ainsi :

* les feuilles sont des arbres : $\mathcal F \subset \mathcal A(\mathcal I, \mathcal F)$
* si g et d appartiennent à $\mathcal A(\mathcal I, \mathcal F)$, et si $e$ appartient à $\mathcal I$, alors $\texttt{Noeud}(e,g,d) \in \mathcal A(\mathcal I, \mathcal F)$

Cela se traduit simplement en OCaml avec un type somme :

```ocaml
type ('i, 'f) ab_strict =
	| Feuille of 'f
	| Noeud of 'i * ('i, 'f) ab_strict * ('i, 'f) ab_strict
```

> 1. Représenter graphiquement les arbres suivants :
>
>     ```ocaml
>     let strict_premier_exemple =
>         Noeud(0,
>             Noeud(4,
>                 Feuille 'i',
>                 Feuille 'n'),
>             Noeud(2,
>                 Feuille 'f',
>                 Feuille 'o'))
>     
>     let strict_deuxieme_exemple =
>         Noeud(1.2,
>             Noeud(9.5,
>                 Noeud(6.7,
>                     Feuille 3,
>                     Feuille 6),
>                 Feuille 3),
>             Feuille 4)
>     
>     let strict_troisieme_exemple =
>         Noeud(0,
>             Noeud(4,
>                 Feuille 11,
>                 Feuille 12),
>             Noeud(2,
>                 Noeud(20,
>                     Feuille 36,
>                     Feuille 37),
>                 Feuille 22))
>     ```
>
> 3. Que renvoie la fonction `mystere` suivante ?
>
>     ```ocaml
>     let rec mystere arbre = match arbre with
>         | Feuille _ -> 0
>         | Noeud (_, g, d) -> 1 + max (mystere g) (mystere d)
>     ```
>
>     Donner sa complexité.
>
> 4. Écrire deux fonctions `nb_feuilles : ('i, 'f) ab_strict -> int` et `nb_noeuds_internes : ('i, 'f) ab_strict -> int` qui renvoient respectivement le nombre de feuilles et le nombre de nœuds internes d'un arbre binaire strict.
>
> 5. Écrire une fonction `produit : (int, int) ab_strict -> int` qui renvoie le produit de toutes les étiquettes d'un arbre binaire d'entiers.
>
> 6. Écrire une fonction `diff_max_profondeur : ('i, 'f) ab_strict -> int` qui renvoie la différence maximale entre les profondeurs de deux feuilles de l'arbre. Donner sa complexité. On pourra s'aider d'une fonction auxiliaire.
>
> 7. (Question difficile, passez la si nécessaire) Écrire une fonction `feuille_basse : ('i, 'f) ab_strict -> 'f` qui renvoie l'étiquette de la feuille située le plus à gauche parmi celles de profondeur maximale. *On ne parcourra qu'une seule fois l'arbre.*

On considère un ensemble $\mathcal E$ (étiquettes des nœuds). On définit l'ensemble $\mathcal A(\mathcal E)$ des arbres binaires non stricts par induction ainsi :

* l'arbre vide $\bot$ appartient à $\mathcal A(\mathcal E)$
* si g et d appartiennent à $\mathcal A(\mathcal E)$, et si $e$ appartient à $\mathcal E$, alors $N(e,g,d) \in \mathcal A(\mathcal E)$

Cela se traduit à nouveau en OCaml avec un type somme :

```ocaml
type 'e ab =
	| Vide
	| Noeud of 'e * 'e ab * 'e ab
```

> 7. Représentez graphiquement l'arbre suivant :
>
>     ```ocaml
>     Noeud('m', Noeud('a', Noeud('o', Vide, Noeud('c', Vide, Vide)), Vide), Noeud('l', Vide, Vide))
>     ```
>
> 9. Que renvoie la fonction `mystere` suivante ?
>
>     ```ocaml
>     let rec mystere arbre etiquette = match arbre with
>         | Vide -> false
>         | Noeud (e, g, d) -> etiquette = e || mystere g etiquette || mystere d etiquette
>     ```
>
>     Donner sa complexité.
>
> 10. Écrire une fonction `est_feuille : 'e ab -> bool` qui détermine si un arbre binaire non strict est une feuille.
>
> 11. Écrire une fonction `egaux_strict_et_non_strict : ('a, 'a) ab_strict -> 'a ab -> bool` qui détermine si deux arbres (un strict et l'autre non) sont égaux, en supposant qu'une feuille d'un arbre strict est égale à un nœud d'un arbre non strict ayant la même étiquette et deux sous-arbres vides.

Pour accéder à un nœud de l'arbre, on descend en partant de la racine et en allant à gauche ou à droite. On peut donc représenter un tel chemin par une liste de déplacements :

```ocaml
type deplacement = Gauche | Droite
type chemin = deplacement list
```

> 11. Écrire une fonction `suit_chemin : 'e ab -> chemin -> 'e option` qui renvoie l’étiquette d’un nœud donné par son chemin. On utilise un type `option` en cas de chemin invalide.
> 13. Écrire une fonction `construit_chemin : 'e ab -> 'e -> chemin option` qui renvoie le chemin d’un nœud de l'arbre donné par son étiquette. On utilise un type `option` dans le cas où l'étiquette n'est pas présente dans l'arbre. *On essaiera d'être efficace*.

### 2. Arbres d'arité quelconque

On considère maintenant des arbres d'arité quelconque :

```ocaml
type 'a arbre_aq = {etiquette : 'a; fils : 'a arbre_aq list}
```

> 13. Vérifier que l'arbre suivant :
>
>     ![](img/arbre.png)
>
>     est bien représenté par :
>
>     ```ocaml
>     let arite_quelconque_premier_exemple =
>         {etiquette = 1; fils = [
>             {etiquette = 2; fils = [
>                 {etiquette = 5; fils = []}
>             ]};
>             {etiquette = 3; fils = [
>                 {etiquette = 6; fils = [
>                     {etiquette = 10; fils = []}
>                 ]};
>                 {etiquette = 7; fils = []}
>             ]};
>             {etiquette = 4; fils = [
>                 {etiquette = 8; fils = []};
>                 {etiquette = 9; fils = []}
>             ]}
>         ]}
>     ```
>
> 14. Écrire les fonctions suivantes sur les arbres d'arité quelconque :
>
>     * `taille : 'a arbre_aq -> int` qui renvoie la taille de l'arbre ;
>     * `hauteur : 'a arbre_aq -> int` qui renvoie la hauteur de l'arbre ;
>     * `appartient : 'a arbre_aq -> 'a -> bool` qui détermine si une étiquette apparaît dans l'arbre ;
>     * `egaux : 'a arbre_aq -> 'a arbre_aq -> bool`  qui détermine si deux arbres sont égaux.
>
>     *Indication : il peut être judicieux d'utiliser des fonctions auxiliaires manipulant des forêts.*

Tout arbre d'arité quelconque peut être transformé en arbre binaire. La manière standard de le faire est la représentation « LCRS » (*left child, right sibling*).

On part d'un arbre d'arité quelconque, par exemple :

![](img/lcrs1.png)

Chaque nœud contient :

* un lien vers la liste chaînée de ses fils, c'est-à-dire vers son fils le plus à gauche (*left child*)
* un lien dans sa propre liste chaînée vers son frère juste à droite (*right sibling*) (sauf la racine qui n'est pas dans une liste)

On représente ces listes chaînées sur ce même exemple :

![](img/lcrs2.png)

La binarisation de l'arbre en découle directement :

* le fils gauche de chaque nœud de l'arbre binaire correspond au fils le plus à gauche du nœud dans l'arbre d'origine
* le fils droit de chaque nœud de l'arbre binaire correspond au frère droit du nœud dans l'arbre d'origine.

Cela donne sur notre exemple :

![](img/lcrs3.png)

Cet algorithme est réversible (essentiellement, nous avons une bijection de l'ensemble des arbres dans l'ensemble des arbres binaires).

> 15. Écrire une fonction `binarise : 'a arbre_aq -> 'a ab` qui convertit un arbre d'arité quelconque en arbre binaire.
> 18. Écrire une fonction `debinarise : 'a ab -> 'a arbre_aq` qui réalise la conversion inverse.
>
> Pour tester vos fonctions, voici l'arbre d'arité quelconque de l'image ci-dessus :
>
> ```ocaml
> let exemple_arbre_a_binariser =
>     {etiquette = 1; fils = [
>         {etiquette = 2; fils = [
>             {etiquette = 5; fils = []};
>             {etiquette = 6; fils = [
>                 {etiquette = 12; fils = [
>                     {etiquette = 16; fils = []};
>                     {etiquette = 17; fils = []};
>                     {etiquette = 18; fils = []}
>                 ]}
>             ]}
>         ]};
>         {etiquette = 3; fils = [
>             {etiquette = 7; fils = []}
>         ]};
>         {etiquette = 4; fils = [
>             {etiquette = 8; fils = [
>                 {etiquette = 13; fils = []};
>                 {etiquette = 14; fils = []}
>             ]};
>             {etiquette = 9; fils = []};
>             {etiquette = 10; fils = [
>                 {etiquette = 15; fils = []}
>             ]};
>             {etiquette = 11; fils = []}
>         ]}
>     ]}
> ```

### 3. Parcours d'arbres

Il y a essentiellement deux manières de parcourir un arbre :

* le parcours en largeur ;
* le parcours en profondeur.

> 17. Écrire trois fonctions qui prennent en paramètre un `(int, int) ab_strict` et affiche respectivement l'ordre de traitement des nœuds dans un parcours en profondeur dans l'ordre préfixe, infixe et postfixe de l'arbre.
> 18. Modifier vos trois fonctions pour renvoyer la liste des étiquettes des nœuds dans l'ordre de leur parcours plutôt que les afficher. On ne se limitera donc plus aux étiquettes de type `int`.
> 19. Écrire deux fonctions réalisant le parcours en largeur d'un `(int, int) ab_strict`. La première fonction affichera les étiquettes, la seconde renverra la liste des étiquettes dans l'ordre de leur parcours. *On pourra utiliser les files implémentées par le module `Queue`.*
> 19. Copier-coller le parcours en largeur ci-dessus et changer les « `Queue` » en « `Stack` ». Tester sur quelques arbres binaires. S'agit-il d'un ordre usuel de parcours de l'arbre ?
> 19. Implémenter les *trois* parcours *existants* sur le type `int arbre` (arbres d'arité quelconque). On écrira une première version affichant les étiquettes et une seconde renvoyant la liste des étiquettes.
> 19. Comparer les complexités des versions affichant les étiquettes avec les complexités des versions renvoyant les étiquettes dans une liste.

## II. Construction d'arbres

On reprend dans cette partie les même types (`('i, 'f) ab_strict`, `'e ab` et `'a arbre_aq`) que dans la partie précédente.

### 1. Construction d'arbres étiquetés

On peut construire un nouvel arbre à partir d'un arbre existant en conservant son squelette mais en changeant ses étiquettes.

> 1. Écrire une fonction `ab_map : ('e -> 'd) -> 'e ab -> 'd ab` de principe similaire à `List.map` mais pour les arbres binaires.
> 2. Écrire une fonction `change_bool : bool arbre_aq -> bool arbre_aq` qui prend en paramètre un arbre $a$ d'arité quelconque dont les étiquettes sont des booléens et renvoie un arbre de même squelette mais dont les étiquettes sont toutes différentes de celles de $a$ (`true` devient `false` et inversement).
> 3. Écrire une fonction `arbre_profondeurs : ('i, 'f) ab_strict -> (int, int) ab_strict` qui prend en paramètre un arbre binaire $a$ et renvoie un autre arbre ayant exactement la même forme mais dont l'étiquette de chaque nœud $n$ a été remplacée par la profondeur de $n$ dans $a$. *On essaiera d'être efficace.*
> 4. Écrire une fonction `arbre_hauteurs : ('i, 'f) ab_strict -> (int, int) ab_strict` qui prend en paramètre un arbre binaire et renvoie un autre arbre ayant exactement la même forme mais dont l'étiquette de chaque nœud a été remplacée par la hauteur du sous-arbre correspondant. *On essaiera d'être efficace (autrement dit, on ne recalculera pas toutes les hauteurs...).*

On peut aussi facilement construire un arbre binaire s'il a un squelette spécifique.

> 5. Écrire une fonction `peigne_gauche` qui prend en paramètre un entier `n` et renvoie un peigne gauche à `n` nœuds dont les étiquettes, lues dans l’ordre *préfixe* du parcours en profondeur, forment la liste `[n, ..., 1]`. Quel est son type ? Sa complexité ?
> 7. Écrire une fonction `peigne_droit` qui prend en paramètre un entier `n` et renvoie un peigne droit à `n` nœuds dont les étiquettes, lues dans l’ordre *postfixe* du parcours en profondeur, forment la liste `[n, ..., 1]`. Quel est son type ? Sa complexité ?
> 8. Écrire une fonction `filiformes` qui prend en paramètre un entier `n > 0` et renvoie la liste de tous les arbres filiformes à `n` nœuds dont les étiquettes, lues dans l’ordre *préfixe* du parcours en profondeur, forment la liste `[n, ..., 1]`.
> 9. Écrire une fonction `parfait` qui prend en paramètre un entier $n$ > 0 et renvoie un arbre parfait à $`2^n-1`$ nœuds, dont les nœuds situés à profondeur $k$ portent l'étiquette $n-k$.
> 10. Quelle est la complexité temporelle de la fonction `parfait` ? Si ce n'est pas $`\mathcal O(n)`$, améliorer la fonction.
> 11. Combien d’espace l’arbre construit par la fonction `parfait` occupe-t-il en mémoire ?

### 2. Cas des arbres complets à gauche

Un arbre binaire complet à gauche peut aisément être représenté par un tableau :

* la racine est stockée à l'indice 0 ;
* pour un nœud stocké à l’indice $i$, son fils gauche est stocké à l’indice $2i+1$ et son fils droit à l’indice
    $2i+2$.

On note $`e_i`$ l'étiquette du nœud d'indice $i$ dans le parcours en largeur d'un arbre complet, on obtient donc la représentation suivante :

![](img/ab_complet_tableau.png){width=50%}

> 11. Montrer que pour un nœud stocké à l’indice $i$ > 0, son père est stocké à l’indice $`\big\lfloor\frac {i-1}2\big\rfloor`$.
>
> 18. Donner le tableau représentant l'arbre complet suivant :
>
>     ![](img/arbre_complet.png){width=30%}
>
> 19. Écrire une fonction `tableau_vers_arbre : 'a array -> 'a ab` qui prend en paramètre un tableau représentant un arbre complet à gauche, et renvoie l'arbre correspondant.
>
> 20. Écrire une fonction faisant l'inverse `arbre_vers_tableau : 'a ab -> 'a array`.

Pour sérialiser un arbre complet à gauche, il suffit de stocker, à la ligne $i$ du fichier, l'étiquette stockée à l'indice $i$ dans la représentation sous forme de tableau de l'arbre.

> 15. Écrire une fonction `serialise_complet : int ab -> string -> unit` qui prend en paramètre un arbre binaire complet à gauche (dont les étiquettes sont des entiers) et le nom d'un fichier et sérialise cet arbre.
> 22. Écrire aussi une fonction `deserialise_complet : string -> int ab`.

### 3. Sérialisation d'arbres binaires

> 17. Proposer deux fonctions `serialise` et `deserialise` sur les arbres binaires stricts (`('i, 'f) ab_strict`). On pourra se limiter à nouveau aux étiquettes de type `int` par exemple.
> 24. Expliquer en quoi il est utile de stocker des arbres construits dans des fichiers plutôt que de les conserver dans le programme lui-même.

## Pour aller plus loin

> Si vous avez terminé, vous pouvez :
>
> * Reprendre les fonctions de la partie II. en C.
> * Reprendre les fonctions du TP sur les arbres en C et les implémenter en OCaml.


---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Source des images : *production personnelle*, *J.B. Bianquis* (LCRS)
