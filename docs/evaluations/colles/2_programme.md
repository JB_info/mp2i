# Programme de colle n° 2

Vous passerez par 6 pendant 2 heures.

Le sujet comportera à la fois des questions de **programmation en C**, à faire sur machine, et des **questions théoriques** à faire au tableau. Dans les deux cas, vous devrez expliciter votre raisonnement **à l'oral**.

Le **programme** est le suivant :

* programmation :
    * processus de compilation, rôles des divers fichiers
    * bases de la programmation en C (impérative et récursive) : fonctions, main, boucles, conditionnelles, ...
    * bibliothèques usuelles (`stdbool.h`, `stdio.h`, `stddef.h`, `stdlib.h`, `stdint.h`, `assert.h`, `string.h`)
    * pointeurs et gestion de la mémoire
    * gestion de fichiers
    * arguments en ligne de commande
    * bonnes pratiques (spécification, programmation défensive, commentaires...)
* jeu de tests :
    * graphe de flot de contrôle
    * écriture d'un jeu de tests complet
    * test exhaustif d'une conditionnelle
* structures de données :
    * types de base prédéfinis (int et variantes, double, bool, char) et opérateurs associés
    * tableaux statiques
    * chaînes de caractères et fonctions associées
    * déclaration de types (`struct` et `typedef`), y compris récursifs
    * implémentations de structures séquentielles (listes, piles, files), avec des maillons chaînés et avec un tableau
* algorithmique :
    * petits algorithmes vus plusieurs fois (ex : exponentiation rapide, algorithme d'Euclide...)
    * algorithmes simples sur les structures séquentielles (extremum, nombre d'occurrences...)
    * algorithmes de tris, en particulier le tri fusion et le tri rapide (avec partition de Lomuto pour les tableaux)
    * méthode « diviser pour régner » (et calculs de complexité)
    * algorithmes gloutons (et preuves d'optimalité simples)
    * programmation dynamique (avec les deux approches)
* analyse d'algorithme : terminaison, correction, complexité spatiale et temporelle (algorithmes impératifs et récursifs)
* représentation des nombres :
    * écriture d'un entier naturel dans une base $`b > 1`$ (théorème, preuve, et algorithmes)
    * complément à 2
    * représentation à virgule flottante et ses limites


L'accent sera porté sur les parties **structures de données** et **algorithmique**. Autrement dit, vous aurez au moins une structure de données à implémenter, et au moins un problème à résoudre à l'aide d'une méthode de type « décomposer un problème en sous-problèmes » (diviser pour régner, glouton, ou programmation dynamique), avec toujours les analyses qui vont avec.


---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)
