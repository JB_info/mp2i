# Planning de passage de colle n° 2

Vous passerez la colle n° 2 par 6 pendant 2 heures.

**Remarque importante :** Les semaines concernées par la colle n°2 (du 10/03 au 04/04), vous n'aurez pas de TP sur le créneau habituel de colles (lundi et mardi 16-18).

Vous trouverez ci-dessous le planning de passage, en fonction de vos groupes de colles habituels.

### MARDI 18 MARS, 16H-18H

* M. DETREZ (salle S1012) : groupes 12 et 16.
* Mme BENOUWT (salle S1010) : groupes 5, 6 et 7.

### MARDI 25 MARS, 16h-18h.

*   Mme BENOUWT (salle S1012) : groupes 11 et 14.

### LUNDI 31 MARS, 16H-18H

* M. DETREZ (salle S1012) : groupes 8 et 13.
* Mme BENOUWT (salle S1010) : groupes 2 et 4.

### MARDI 1 AVRIL, 16H-18H

* M. DETREZ (salle S1012) : groupes 1 et 3.
* Mme BENOUWT (salle S1010) : groupes 9 et 10.


---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)
