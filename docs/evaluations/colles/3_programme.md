# Programme de colle n° 3

Vous passerez par 3 pendant 1 heure, intégralement au tableau.

Le programme est le suivant :

* représentation des nombres : conversion d'un entier naturel d'une base à une autre, représentation des entiers relatifs avec le complément à 2, représentation à virgule flottante et ses limites
* structures de données séquentielles : tableaux, listes, piles, files, tableaux associatifs implémentés par une table de hachage
* algorithmique :
    * exploration exhaustive (recherche par force brute, backtracking)
    * décomposition d'un problème en sous-problème (gloutons, programmation dynamique, diviser pour régner, dichotomie)
    * analyse d'algorithmes : terminaison, correction, complexité spatiale et temporelle (algorithmes impératifs et récursifs)
* arbres :
    * définitions inductives des arbres binaires stricts et non stricts
    * vocabulaire (nœud, branche, étiquette, nœud interne, feuille, racine, fils, père, sous-arbre, arité, profondeur, ...)
    * définition inductive d'une fonction sur l'ensemble des arbres (taille, hauteur, ...)
    * arbre d'arité quelconque (définition, vocabulaire, ...) et binarisation d'un arbre d'arité quelconque
    * utilisation des arbres sur un problème concret : arbres d'expressions arithmétiques, arbres syntaxiques, tries, ...
    * parcours d'arbres et d'arbres binaires (largeur, et profondeur dans les ordres préfixe, postfixe, infixe)
    * sérialisation d'un arbre binaire (et reconstruction de l'arbre à partir de son parcours)
    * preuve par induction structurelle sur les arbres
* graphes :
    * définitions des graphes orientés et non orientés $G = (S,A)$
    * vocabulaire : sommet, arc, arête, boucle, degré (entrant et sortant), sous-graphe, sous-graphe induit
    * notion de chemin d'un sommet à un autre, chemin simple et élémentaire, cycle
    * accessibilité, connexité, forte connexité, composantes connexes et fortement connexes
    * représentation par matrice d'adjacence et par liste d'adjacence
    * graphes particuliers : graphe complet, graphe orienté acyclique, graphe biparti, ...
    * parcours de graphes (en largeur et en profondeur)
    * applications des parcours de graphes : recherche de chemin, distance entre deux sommets, détection de cycle, connexité, recherche des composantes connexes, bicolorabilité d’un graphe
    * tri topologique d'un graphe orienté acyclique
    * arbre en tant que graphe connexe acyclique, notion de forêt
    * arbre couvrant, arborescence d'un parcours
    * pondération d'un graphe et applications
* ordre et induction :
    * relation d'ordre, ensemble ordonné, ordre total, prédécesseur et successeur, élément minimal
    * représentation d'un ensemble ordonné par un graphe orienté acyclique
    * ordre bien fondé
    * ordre produit, ordre lexicographique
    * ensemble inductif (système d'assertions et de règles d'inférences)
    * définition de fonctions par induction
    * preuve par induction structurelle
* logique :
    * variables propositionnelles, connecteurs logiques
    * formules propositionnelles : définition par induction, représentation comme un arbre
    * vocabulaire : hauteur, taille, sous-formule, ...
    * quantificateurs universel et existentiel
    * substitution d’une variable, table de vérité, valuation
    * tautologie, antilogie, formule satisfiable
    * équivalence logique, conséquence logique

L'accent sera porté sur les **arbres**, les **graphes** et la **logique**.


---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Source des images : *production personnelle*
