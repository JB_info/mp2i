# Programme de colle n° 1

Vous passerez par 6 pendant 2 heures.

Le sujet comportera à la fois des questions pratiques de **programmation en OCaml**, à faire sur machine, et des **questions théoriques** à faire au tableau. Dans les deux cas, vous devrez expliciter votre raisonnement **à l'oral**.

Le **programme** est le suivant :

* programmation :
    * bases de la programmation fonctionnelle en OCaml
    * bases de la programmation impérative en OCaml
    * bonnes pratiques (spécification, commentaires...)

* récursivité
* structures de données :
    * types de base prédéfinis (int, float, bool, char, string) et opérateurs et fonctions associés
    * déclaration de types (somme, produit, alias, y compris types mutables et/ou récursifs)
    * tableaux et listes (y compris multidimensionnels) et fonctions des modules `Array` et `List`
    
* algorithmique :
    * petits algorithmes vus plusieurs fois (ex : exponentiation rapide, algorithme d'Euclide...)
    * algorithmes simples sur les tableaux et les listes (moyenne, extremum, nombre d'occurrences...)
    * recherche dichotomique
    * algorithmes de tris quadratiques (sélection, insertion, à bulles)

* analyse d'algorithme : terminaison, correction, complexité spatiale et temporelle (algorithmes impératifs et récursifs)

Pour réussir la partie pratique, il faut **maîtriser les « fondamentaux » des TP** suivants :

* Bases d'OCaml
* Listes OCaml
* Déclaration de types en OCaml
* Programmation impérative en OCaml
* Tableaux en OCaml
* Premiers tris et leur analyse

Pour réussir la partie théorique, il faut **maîtriser le chapitre « Analyse d'algorithmes »** et :

* s'entraîner à refaire les TD sur la terminaison, correction, et complexité
* faire (ou refaire) l'analyse des algorithmes à maîtriser absolument (factorielle, exponentiation rapide, recherche dichotomique, tris, etc.)


---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)
