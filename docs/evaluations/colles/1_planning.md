# Planning de passage de colle n° 1

Vous passerez la colle n° 1 par 6 pendant 2 heures.

**Remarques importantes :**

* Les trois semaines concernées par la colle n°1 (du 09/12 au 11/01), vous n'aurez pas de TP sur le créneau habituel de colles (lundi et mercredi après-midis).
* Les TD d'info du jeudi 19 décembre seront exceptionnellement annulés. En contrepartie, vous aurez cours le mercredi 18 décembre 14h-16h (en B307).

Vous trouverez ci-dessous le planning de passage, en fonction de vos groupes de colles habituels.

### MERCREDI 11 DÉCEMBRE, 14H-16H

* M. DETREZ (salle S1012) : groupes 4 et 6.
* Mme BENOUWT (salle S1010) : groupes 8 et 10.

### JEUDI 19 DÉCEMBRE, 14H-16H

* M. DETREZ (salle S1012) : groupes 1 et 3.
* Mme BENOUWT (salle S1010) : groupes 9 et 13.

### MERCREDI 8 JANVIER, 13H-15H

* M. DETREZ (salle S1012) : groupes 5 et 7.
* Mme BENOUWT (salle S1010) : groupes 11 et 15.

### MERCREDI 8 JANVIER, 15H-17H

* M. DETREZ (salle S1012) : groupes 2 et 14.
* Mme BENOUWT (salle S1010) : groupes 12 et 16.


---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)
