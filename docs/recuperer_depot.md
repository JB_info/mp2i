# Comment récupérer une copie d'un dépôt

Ce dépôt va très probablement être amené à changer l'année prochaine. Je ne conserverai pas une copie des documents de cette année : certains TP ou TD pourront être modifiés voire supprimés et vous ne trouverez donc plus les versions sur lesquelles vous avez travaillé. De même pour le dépôt contenant les corrigés.

Je vous conseille donc de faire une copie des deux dépôts avant que je commence à le modifier. Vous en aurez besoin l'année prochaine lorsque vous réviserez les concours, ou même cet été si vous souhaitez retravailler un TP ou un TD.

Pour cela, plusieurs options s'offrent à vous :

* Allez sur la page d'accueil du dépôt, puis en haut à droite cliquez sur le symbole de téléchargement, et sélectionnez le format souhaité (`.zip`)  par exemple. *Inconvénient : il vous faudra installer un éditeur markdown pour pouvoir lire les documents sur votre ordinateur (ils sont presque tous au format `.md`).*
* Connectez-vous à votre compte `framagit`, allez sur la page d'accueil du dépôt, puis en haut à droit cliquez sur « Cloner », copiez l'adresse du dépôt et tapez la commande `git clone [adresse copiée]`. *Inconvénient : il ne faudra surtout plus faire de git pull depuis ce clone sinon vous perdrez les fichiers de cette année.*
* Connectez-vous à votre compte `framagit`, allez sur la page d'accueil du dépôt, puis tout en haut cliquez sur « Créer une divergence » (ou « `Fork` »). Cochez la case "Niveau de visibilité privé" en bas, donnez le nom que vous voulez au dépôt puis confirmez la création. *Avantage : vous possédez maintenant une copie du dépôt sur votre compte (que vous pouvez bien sûr cloner si vous le souhaitez) : cette copie est indépendante du dépôt d'origine et ne sera donc pas impactée par les modifications de ce dépôt. De plus vous pouvez visualiser tous les fichiers en ligne depuis votre compte sans rien installer.*

Je vous conseille la troisième méthode.

![](img/bonnes_vacances.png)


---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Source des images : *production personnelle*