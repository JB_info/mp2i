# Calendrier du semestre 1

*Remarque : Le calendrier suivant est prévisionnel, il est susceptible d'évoluer.*

Volume horaire :

* Cours : 2h toutes les semaines, en classe entière

* TD : 1h toutes les semaines, en demi-groupe

* TP : 2h toutes les 2 semaines, en tiers de classe

* Colles : plusieurs formats :
  
  - 2h de TP en demi-groupe
  - 2h d'interrogation avec ordinateur, par 6
  - 1h d'interrogation orale au tableau, par 3
  
  Au premier semestre, le format TP en demi-groupe sera privilégié pour les colles. Vous aurez une unique « vraie » interrogation à la fin du semestre, de 2h avec ordinateur.



|    Semaine    |                            Cours                             |                              TD                              |                              TP                              |                            Colles                            |                           Devoirs                            |
| :-----------: | :----------------------------------------------------------: | :----------------------------------------------------------: | :----------------------------------------------------------: | :----------------------------------------------------------: | :----------------------------------------------------------: |
| 05/09 - 07/09 | 1. [Introduction à l'informatique](../cours/1_intro_ordi_os_sgf.md) (I. II. III.) |                1. [Python](../td/1_python.md)                |            -> TD : [1. Python](../td/1_python.md)            |                              -                               |                              -                               |
| 12/09 - 14/09 | 1. [Introduction à l'informatique](../cours/1_intro_ordi_os_sgf.md) (IV.) |    2. [Système de fichiers](../td/2_systeme_fichiers.md)     | 1. [Maîtrise du terminal](../tp/1_terminal.md) : Si vous en avez une, pensez à ramener une clé USB. |                              -                               |                              -                               |
| 19/09 - 21/09 | - 1. [Introduction à l'informatique](../cours/1_intro_ordi_os_sgf.md) (V.)<br />- 2. [Programmation fonctionnelle en OCaml](../cours/2_ocaml_fonctionnel.md) (I. à VI.) |  3. [Bases fonctionnelles d'OCaml](../td/3_bases_ocaml.md)   | 1. [Maîtrise du terminal](../tp/1_terminal.md) : Si vous en avez une, pensez à ramener une clé USB. | 2. [Bases fonctionnelles d'OCaml](../tp/ocaml/2_bases_ocaml.md) |          1. IE (Cours 1 : III. IV. et TD 2 : 1 à 5)          |
| 26/09 - 28/09 | - 2. [Programmation fonctionnelle en OCaml](../cours/2_ocaml_fonctionnel.md) (VII. à IX.)<br />- 3. [Récursivité](../cours/3_recursivite.md) (I. II. III.) | -> Cours : [3. Récursivité](../cours/3_recursivite.md) (III. IV.) | - 3. [Découverte du C et compilation](../tp/c/3_bases_c.md)<br />- Bonus. [Exercices d'entraînement au C](../tp/c/3_6_exos_c.md) |   4. [Récursivité et listes](../tp/ocaml/4_recursivite.md)   |            2. IE (Cours 1 : III. IV. V. et TP 1)             |
| 03/10 - 05/10 |    - 3. [Récursivité](../cours/3_recursivite.md) (IV. V.)    |      4. [Récursivité en OCaml](../td/4_recursivite.md)       | - 3. [Découverte du C et compilation](../tp/c/3_bases_c.md)<br />- Bonus. [Exercices d'entraînement au C](../tp/c/3_6_exos_c.md) | 5. [Manipulation de listes (polynômes)](../tp/ocaml/5_polynomes.md) | - 3. IE (TP 2, TD 3, Cours 2)<br />- 1. [DS](../evaluations/ds/correction.md) (Chapitres 1 à 3 et TD/TP associés) |
| 10/10 - 12/10 | 4. [Programmation impérative](../cours/4_imperatif.md) (Partie A) | 5. [Impératif en OCaml](../td/5_imperatif_ocaml.md) : finir l'exercice 1 pour la semaine prochaine | - 6. [Tableaux statiques](../tp/c/6_tableaux_statiques.md)<br />- Bonus. [Exercices d'entraînement au C](../tp/c/3_6_exos_c.md) |      7. [Déclarations de types](../tp/ocaml/7_types.md)      |           4. IE (TP 4, TP 5 I. II., TD 4, Cours 3)           |
| 17/10 - 19/10 |   -> TP 7. [Déclarations de types](../tp/ocaml/7_types.md)   |     5. [Impératif en OCaml](../td/5_imperatif_ocaml.md)      | - 6. [Tableaux statiques](../tp/c/6_tableaux_statiques.md)<br />- Bonus. [Exercices d'entraînement au C](../tp/c/3_6_exos_c.md) |      7. [Déclarations de types](../tp/ocaml/7_types.md)      |                         5. IE (TP 3)                         |
|   Vacances    |                              -                               |                              -                               |                              -                               |                              -                               | 1. [DM](../evaluations/dm/correction.md) : à déposer sur l'ENT avant dimanche 06/11 23h59. |
| 07/11 - 09/11 | 4. [Programmation impérative](../cours/4_imperatif.md) (Partie B) |        -> Bilan et corrigé du DS 1 : ramener le sujet        |            8. [Pointeurs](../tp/c/8_pointeurs.md)            | -> Cours ([Analyse d'algorithmes](../cours/5_analyse_algos.md) ) |                         6. IE (TP 7)                         |
| 14/11 - 16/11 | 5. [Analyse d'algorithmes](../cours/5_analyse_algos.md) (I. II.) | -> Cours : 5. [Analyse d'algorithmes](../cours/5_analyse_algos.md) (III.) |            8. [Pointeurs](../tp/c/8_pointeurs.md)            | 9. [Programmation impérative et tableaux](../tp/ocaml/9_imperatif.md) |                         7. IE (TP 6)                         |
| 21/11 - 23/11 | 5. [Analyse d'algorithmes](../cours/5_analyse_algos.md) (III. IV.) | 6. [Terminaison - correction](../td/6_terminaison_correction.md) | - [Introduction à git](../tp/git.md) : créer un compte sur framagit au préalable <br />- 10. [Gestion de la mémoire et tableaux](../tp/c/10_memoire_tableaux.md) |          11. [Matrices](../tp/ocaml/11_matrices.md)          | 8. IE (TP 9, TD 5, Cours 4 partie A)<br />2. [DS](../evaluations/ds/correction.md) (Chapitres 2 à 4 et TD/TP associés) |
| 28/11 - 30/11 | 5. [Analyse d'algorithmes](../cours/5_analyse_algos.md) (IV. V.) | -> Cours ([Analyse d'algorithmes](../cours/5_analyse_algos.md)) | - [Introduction à git](../tp/git.md) : créer un compte sur framagit au préalable <br />- 10. [Gestion de la mémoire et tableaux](../tp/c/10_memoire_tableaux.md) |         12. [Quelques tris](../tp/ocaml/12_tris.md)          |                         9. IE (TP 8)                         |
| 05/12 - 07/12 | 5. [Analyse d'algorithmes](../cours/5_analyse_algos.md) (V.) | 6. [Terminaison - correction](../td/6_terminaison_correction.md) |      13. [Chaînes de caractères](../tp/c/13_chaines.md)      |         12. [Quelques tris](../tp/ocaml/12_tris.md)          | 10. IE (TP 11 jusque "Opérations générales" de l'exercice ADN) |
| 12/12 - 14/12 | 6. [Structures séquentielles](../cours/6_structures_sequentielles.md) (I. II. III.) | -> Cours ([Structures séquentielles](../cours/6_structures_sequentielles.md)) |      13. [Chaînes de caractères](../tp/c/13_chaines.md)      | 14. [Piles et Files (manipulations)](../tp/ocaml/14_piles_files.md) |                   11. IE (TP 12, Cours 5)                    |
|   Vacances    |                              -                               |                              -                               |                              -                               |                              -                               | 2. [DM](../evaluations/dm/correction.md) : à déposer sur l'ENT avant dimanche 01/01 15h00. |
| 03/01 - 04/01 |                              -                               |        -> Bilan et corrigé du DS 2 : ramener le sujet        | 15. [Ligne de commande et structures simples](../tp/c/15_argv_structures.md) |       [colle 1](../evaluations/colles/1_programme.md)        |                              -                               |
| 09/01 - 11/01 | - 6. [Structures séquentielles](../cours/6_structures_sequentielles.md) (III. IV.) |            7. [Complexité](../td/7_complexite.md)            | 15. [Ligne de commande et structures simples](../tp/c/15_argv_structures.md) |       [colle 1](../evaluations/colles/1_programme.md)        | 3. [DS](../evaluations/ds/correction.md) (Chapitres 2 à 6 et TD/TP associés) |
| 16/01 - 18/01 | - 6. [Structures séquentielles](../cours/6_structures_sequentielles.md) (IV.)<br />- 7. [Validation de programmes](../cours/7_validation.md) (I.) |            7. [Complexité](../td/7_complexite.md)            | 16. [Structures récursives](../tp/c/16_structures_recursives.md) |       [colle 1](../evaluations/colles/1_programme.md)        |                              -                               |
| 23/01 - 25/01 | 7. [Validation de programmes](../cours/7_validation.md) (I. II.) |  8. [Dérécursivation et Hanoï](../td/8_derecursivation.md)   | 16. [Structures récursives](../tp/c/16_structures_recursives.md) |       [colle 1](../evaluations/colles/1_programme.md)        |                              -                               |

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Source des images : *production personnelle*
