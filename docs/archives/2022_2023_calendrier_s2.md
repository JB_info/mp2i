# Calendrier du semestre 2

*Remarque : Le calendrier suivant est prévisionnel, il est susceptible d'évoluer.*

Volume horaire :
* Cours : 4h toutes les semaines, en classe entière
* TD : 1h toutes les semaines, en demi-groupe
* TP : 2h toutes les 2 semaines, en tiers de classe
* Colles : plusieurs formats :
  - 2h de TP en demi-groupe
  - 2h d'interrogation avec ordinateur, par 6
  - 1h d'interrogation orale au tableau, par 3
  
  Au second semestre, le format TP en demi-groupe sera privilégié pour les colles. Vous aurez deux « vraies » interrogations en avril et mai.

| Semaine | Cours | TD | TP | Colles | Devoirs |
| :---: | :---: | :---: | :---: | :---: | :---: |
| 30/01 - 01/02 | - 7. [Validation de programmes](../cours/7_validation.md) (II. III. IV.) | 9. [Listes chaînées](../td/9_listes_chainees.md) | 17. [Piles et files](../tp/c/17_piles_files.md) | - 18. Salle 1012 : [Implémentations de piles et de files + compilation](../tp/ocaml/18_implementation_piles_files.md)<br />- Salle 1010 : [Présentation du projet, choix des sujets...](../evaluations/dm/projets/ocaml.md) | 1. IE (TP 15) |
| 06/02 - 08/02 | 8. [Représentation des nombres](../cours/8_representation_nombres.md) (I. II.) | 10. [Tests](../td/10_tests.md) | 17. [Piles et files](../tp/c/17_piles_files.md) | - 18. Salle 1012 : [Implémentations de piles et de files + compilation](../tp/ocaml/18_implementation_piles_files.md)<br />- Salle 1010 : [Présentation du projet, choix des sujets...](../evaluations/dm/projets/ocaml.md) | 2. IE (TP 16) |
| Vacances | - | - | - | - | 3. [DM](../evaluations/dm/correction.md) : mini-projet OCaml à déposer sur git pour le 04/03 |
| 27/02 - 01/03 | - 8. [Représentation des nombres](../cours/8_representation_nombres.md) (II. III.)<br />- 9. [Décomposition d'un problème en sous-problèmes](../cours/9_decomposition_probleme.md) (I.) | 11. [Représentation des nombres](../td/11_representation.md) | 19. [Diviser pour régner](../tp/c/19_diviser_pour_regner.md) | 20. [Plus de tris](../tp/ocaml/20_tris_efficaces.md) | 3. IE (TP 17 & 18, Cours 6) |
| 06/03 - 08/03 | 9. [Décomposition d'un problème en sous-problèmes](../cours/9_decomposition_probleme.md) (I. II. III.) | 12. [Algorithme de Karatsuba](../td/12_karatsuba.md) | 19. [Diviser pour régner](../tp/c/19_diviser_pour_regner.md) | 21. [Distance minimale dans un nuage de points](../tp/ocaml/21_points_proches.md) | 4. [DS](../evaluations/ds/correction.md)  (Chapitres 5 à 8 et TD/TP associés) |
| 13/03 - 15/03 | - 9. [Décomposition d'un problème en sous-problèmes](../cours/9_decomposition_probleme.md) (III.) | 13. [Chemin de poids maximal](../td/13_chemin_maximal.md) | 22. [Problèmes d'ordonnancement](../tp/c/22_ordonnancement.md) | 23. [Compter les briques](../tp/ocaml/23_compter_les_briques.md) | 4. IE (TP 20) |
| 20/03 - 22/03 | 10. [Arbres](../cours/10_arbres.md) (I. II.) | 14. [Problèmes d'optimisation](../td/14_problemes_optimisation.md) | 22. [Problèmes d'ordonnancement](../tp/c/22_ordonnancement.md) | 24. [Arbres](../tp/ocaml/24_arbres.md) |                     5. IE (TD 11 & 12)                      |
| 27/03 - 29/03 | 10. [Arbres](../cours/10_arbres.md) (II. III.) | 15. [Arbres](../td/15_arbres.md) | 25. [Gestion de fichiers](../tp/c/25_fichiers.md) | 24. [Arbres](../tp/ocaml/24_arbres.md) | 6. IE (TD 13 & 14) |
| 03/04 - 05/04 | - 10. [Arbres](../cours/10_arbres.md) (III. IV.)<br />- 11. [Exploration exhaustive](../cours/11_exploration_exhaustive.md) (I.) | 15. [Arbres](../td/15_arbres.md) | 25. [Gestion de fichiers](../tp/c/25_fichiers.md) | 26. [Sudoku](../tp/ocaml/26_sudoku.md) | 7. IE (TP 24) |
| 11/04 - 12/04 | - 11. [Exploration exhaustive](../cours/11_exploration_exhaustive.md) (II.)<br />- 12. [Graphes](../cours/12_graphes.md) (I.) | - | 27. [Arbres et Graphes](../tp/c/27_arbres_graphes.md) | 28. [Graphes](../tp/ocaml/28_graphes.md) | 5. [DS](../evaluations/ds/correction.md)  (Chapitres 8 à 11 et TD/TP associés) |
| Vacances | - | - | - | - | 4. [DM](../evaluations/dm/correction.md) : [mini-projet C](../evaluations/dm/projets/c.md) à déposer sur git pour le 06/05 |
| 02/05 - 03/05 | 12. [Graphes](../cours/12_graphes.md) (I.) | - | 27. [Arbres et Graphes](../tp/c/27_arbres_graphes.md) | [colle 2](../evaluations/colles/2_programme.md) | 8. IE (TP 25 & 28) |
| 09/05 - 10/05 |       12. [Graphes](../cours/12_graphes.md) (I. II.)       | - | 29. [Logique](../tp/ocaml/29_logique.md) | [colle 2](../evaluations/colles/2_programme.md) | 6. [DS](../evaluations/ds/correction.md)  (Chapitres 9 à 12 et TD/TP associés) |
| 15/05 - 17/05 | - 12. [Graphes](../cours/12_graphes.md) (II. III. IV.)<br />- 13. [Ordre et induction](../cours/13_ordre_induction.md) | 16. [Graphes](../td/16_graphes.md) | 29. [Logique](../tp/ocaml/29_logique.md) | [colle 2](../evaluations/colles/2_programme.md) | - |
| 22/05 - 24/05 |                         14. [Logique](../cours/14_logique.md) (I. II.)                         | 17. [Logique](../td/17_logique.md) | 30. [ABR, ARN, Tas](../tp/c/30_abr_arn_tas.md) | - [colle 2](../evaluations/colles/2_programme.md)<br />- [colle 3](../evaluations/colles/3_programme.md) | - |
| 30/05 - 31/05 | 14. [Logique](../cours/14_logique.md) (II. III.) | - | 30. [ABR, ARN, Tas](../tp/c/30_abr_arn_tas.md) | [colle 3](../evaluations/colles/3_programme.md) | - |
| 05/06 - 07/06 | - 14. [Logique](../cours/14_logique.md) (III.)<br />- 15. [Structures à l'aide d'arbres](../cours/15_abr_arn_tas.md) (I. II.) | 18. [Structures à l'aide d'arbres](../td/18_abr_tas.md) | 31. [Bases de données](../tp/31_bdd.md) : téléchargez [DB Browser for SQLite](https://sqlitebrowser.org/dl/) si vous souhaitez utiliser votre ordinateur personnel | [colle 3](../evaluations/colles/3_programme.md) | 7. [DS](../evaluations/ds/correction.md)  (Chapitres 12 à 15 et TD/TP associés) |
| 12/06 - 14/06 | - 15. [Structures à l'aide d'arbres](../cours/15_abr_arn_tas.md) (II.)<br />- 16. [Plus court chemin dans un graphe pondéré](../cours/16_chemin_pondere.md) (I. II. III.) | 19. [Plus court chemin dans un graphe](../td/19_plus_court_chemin.md) | 31. [Bases de données](../tp/31_bdd.md) : téléchargez [DB Browser for SQLite](https://sqlitebrowser.org/dl/) si vous souhaitez utiliser votre ordinateur personnel | [colle 3](../evaluations/colles/3_programme.md) | - |
| 19/06 - 21/06 | - 16. [Plus court chemin dans un graphe pondéré](../cours/16_chemin_pondere.md) (III.)<br />- 17. [Bases de données](../cours/17_bdd.md) (I. II.) <br /> - 18. [Algorithmique du texte](../cours/18_algo_texte.md) (I. II.) | 20. [Bases de données](../td/20_bdd.md) |                  32. [Algorithmique du texte](../tp/32_algo_texte.md)                  | - | - |
| 26/06 - 28/06 | -  | - |                  -                  | - | [Récupérez une copie de ce dépôt avant le 07/07](../recuperer_depot.md) |
| Vacances | - | - | - | - | - |

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Source des images : *production personnelle*
