# Calendrier du semestre 2

*Remarque : Le calendrier suivant est prévisionnel, il est susceptible d'évoluer.*

Volume horaire :
* Cours : 4h toutes les semaines, en classe entière
* TD : 1h toutes les semaines, en demi-groupe
* TP : 2h toutes les 2 semaines, en tiers de classe
* Colles : plusieurs formats :
  - 2h de TP en demi-groupe
  - 2h d'interrogation avec ordinateur, par 6
  - 1h d'interrogation orale au tableau, par 3
  
  Au second semestre, le format TP en demi-groupe sera privilégié pour les colles. Vous aurez deux « vraies » interrogations en mars et mai.

| Semaine | Cours | TD | TP | Colles | Devoirs |
| :---: | :---: | :---: | :---: | :---: | :---: |
| 05/02 - 10/02 | - 7. [Validation de programmes](../cours/7_validation.md)<br />- 8. [Décomposition d'un problème en sous-problèmes](../cours/8_decomposition_probleme.md) | -> Cours | 18. [Gestion de fichiers](../tp/fichiers.md) | - | 1. IE (TP 15, TD 7, cours 6) |
| 12/02 - 17/02 | - 8. [Décomposition d'un problème en sous-problèmes](../cours/8_decomposition_probleme.md)<br />- 9. [Représentation des nombres](../cours/9_representation_nombres.md) | 9. [Tests](../td/tests.md) | 18. [Gestion de fichiers](../tp/fichiers.md) | 19. [Tris efficaces et leur analyse](../tp/tris_efficaces.md) | 2. IE (TP 17, cours 6) |
| 19/02 - 24/02 | 9. [Représentation des nombres](../cours/9_representation_nombres.md) | 10. [Chemin de poids maximal](../td/chemin_maximal.md) | 21. [Problèmes d'optimisation](../tp/c/problemes_optimisation.md) | 20. [Distance minimale dans un nuage de points](../tp/ocaml/points_proches.md) | 3. IE (TP 19, cours 8) |
| Vacances | - | - | - | - | 3. [DM](../evaluations/dm/correction.md) (à déposer sur git pour le 11/03) |
| 11/03 - 16/03 | - 9. [Représentation des nombres](../cours/9_representation_nombres.md)<br />- 10. [Arbres](../cours/10_arbres.md) | 11. [Sélection d'activité](../td/selection_activite.md) | 21. [Problèmes d'optimisation](../tp/c/problemes_optimisation.md) | 22. [Compter les briques](../tp/ocaml/denombrer_briques.md) | - |
| 18/03 - 23/03 | 10. [Arbres](../cours/10_arbres.md) | 12. [Représentation des nombres](../td/representation_nombres.md) | 23. [Arbres binaires en C](../tp/c/arbres_binaires.md) | [Colle 2](../evaluations/colles/2_programme.md) |                     4. [DS](../evaluations/ds/correction.md) (cours 4 à 9 (I.), TD jusqu'au 11, TP jusqu'au 22)                     |
| 25/03 - 30/03 | - 10. [Arbres](../cours/10_arbres.md)<br />- 11. [Exploration exhaustive](../cours/11_exploration_exhaustive.md) | 13. [Algorithme de Karatsuba](../td/karatsuba.md) | 23. [Arbres binaires en C](../tp/c/arbres_binaires.md) | [Colle 2](../evaluations/colles/2_programme.md) | 4. IE (TP 21, TD 10, TD 11, cours 8) |
| 02/04 - 06/04 | - 11. [Exploration exhaustive](../cours/11_exploration_exhaustive.md)<br />- 12. [Graphes](../cours/12_graphes.md) | 14. [Arbres](../td/arbres.md) | 24. [Arbres en OCaml](../tp/ocaml/arbres.md) | [Colle 2](../evaluations/colles/2_programme.md) | 5. IE (TD 12, TD 13, cours 9) |
| 08/04 - 13/04 | 12. [Graphes](../cours/12_graphes.md) | 15. [Parcours d'arbres](../td/arbres_parcours.md) | 24. [Arbres en OCaml](../tp/ocaml/arbres.md) | 25. [Sudoku](../tp/ocaml/sudoku.md) | 5. [DS](../evaluations/ds/correction.md) (cours 6 à 11, TD jusqu'au 15, TP jusqu'au 24) |
| 15/04 - 20/04 | - 12. [Graphes](../cours/12_graphes.md)<br />- 13. [Ordre et induction](../cours/13_ordre_induction.pdf) | 16. [Raisonner sur les graphes](../td/graphes_raisonnement.md) | - | 26. [Graphes en OCaml](../tp/ocaml/graphes.md) | 6. IE (TP 23, TP 24, TD 14, TD 15, cours 10) |
| Vacances | - | - | - | - | 4. [DM](../evaluations/dm/correction.md) (à déposer sur git pour le 12/05) |
| 06/05 - 11/05 | - | Correction des exercices du chapitre 13. | - |                       27. [Graphes en C](../tp/c/graphes.md)                       | - |
| 13/05 - 18/05 | 14. [Logique](../cours/14_logique.md) | 17. [Parcours de graphes](../td/graphes_parcours.md) | 28. [Bases de données](../tp/bdd.md) (téléchargez [DB Browser for SQLite](https://sqlitebrowser.org/dl/) si vous souhaitez utiliser votre ordinateur personnel) | -> Cours | 6. [DS](../evaluations/ds/correction.md) (cours 11 à 13, TD jusqu'au 17, TP jusqu'au 27) |
| 21/05 - 25/05 | 14. [Logique](../cours/14_logique.md) | 18. [Syntaxe et sémantique des formules logiques](../td/logique_syntaxe_semantique.md) | 28. [Bases de données](../tp/bdd.md) (téléchargez [DB Browser for SQLite](https://sqlitebrowser.org/dl/) si vous souhaitez utiliser votre ordinateur personnel) | - 29. [Logique propositionnelle](../tp/ocaml/logique.md)<br />- [Colle 3](../evaluations/colles/3_programme.md) | 7. IE (TP 26, TP 27, TD 16, TD 17, cours 12) |
| 27/05 - 01/06 | 15. [Bases de données](../cours/15_bdd.md) | 19. [Problème SAT](../td/logique_probleme_sat.md) | 30. [ABR et ARN](../tp/c/abr_arn.md) | - 29. [Logique propositionnelle](../tp/ocaml/logique.md)<br />- [Colle 3](../evaluations/colles/3_programme.md) | - |
| 03/06 - 08/06 | 16. [Structures à l'aide d'arbres binaires](../cours/16_abr_arn_tas.md) | 20. [Bases de données](../td/bdd.md) | 30. [ABR et ARN](../tp/c/abr_arn.md) | - 31. [Tas](../tp/c/tas.md) <br />- [Colle 3](../evaluations/colles/3_programme.md) | 8. IE (TD 18, TD 19, cours 14) |
| 10/06 - 15/06 | - 16. [Structures à l'aide d'arbres binaires](../cours/16_abr_arn_tas.md)<br />- 17. [Plus court chemin dans un graphe pondéré](../cours/17_chemin_pondere.md) | 21. [Arbres binaires de recherche et arbres bicolores](../td/abr_arn.md) | -> TD 23. [Algorithmique du texte](../algo_texte.md) | - 31. [Tas](../tp/c/tas.md) <br />- [Colle 3](../evaluations/colles/3_programme.md) | 7. [DS](../evaluations/ds/correction.md) (cours 13 à 16, TD jusqu'au 21, TP jusqu'au 31) |
| 17/06 - 22/06 | - 17. [Plus court chemin dans un graphe pondéré](../cours/17_chemin_pondere.md)<br />- 18. [Algorithmique du texte](../cours/18_algo_texte.md) | 22. [Plus court chemin dans un graphe](../td/plus_court_chemin.md) | -> TD 23. [Algorithmique du texte](../algo_texte.md) | -> Cours | - |
| 24/06 - 29/06 | - | - | - | - | [Récupérez une copie des dépôts avant le 07/07](../recuperer_depot.md) |
| Vacances | - | - | - | - | - |

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Source des images : *production personnelle*
