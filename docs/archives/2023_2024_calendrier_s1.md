# Calendrier du semestre 1

*Remarque : Le calendrier suivant est prévisionnel, il est susceptible d'évoluer.*

Volume horaire :

* Cours : 2h toutes les semaines, en classe entière

* TD : 1h toutes les semaines, en demi-groupe

* TP : 2h toutes les 2 semaines, en tiers de classe

* Colles : plusieurs formats :
  
  - 2h de TP en demi-groupe
  - 2h d'interrogation avec ordinateur, par 6
  - 1h d'interrogation orale au tableau, par 3
  
  Au premier semestre, le format TP en demi-groupe sera privilégié pour les colles. Vous aurez une unique « vraie » interrogation à la fin du semestre, de 2h avec ordinateur.



|    Semaine    |                            Cours                             |                              TD                              |                              TP                              |                            Colles                            |                           Devoirs                            |
| :-----------: | :----------------------------------------------------------: | :----------------------------------------------------------: | :----------------------------------------------------------: | :----------------------------------------------------------: | :----------------------------------------------------------: |
| 04/09 - 09/09 | 1. [Architecture matérielle et logicielle d'un ordinateur](../cours/1_archi.md) |                              -                               |                              -                               |                           -> Cours                           |                              -                               |
| 11/09 - 16/09 | 1. [Architecture matérielle et logicielle d'un ordinateur](../cours/1_archi.md) |   1. [Bases d'algorithmique](../td/bases_algorithmique.md)   | 2. [Maîtrise du terminal](../tp/terminal.md) : Si vous en avez une, pensez à ramener une clé USB. | 1. [Bases fonctionnelles d'OCaml](../tp/ocaml/bases_fonctionnelles.md) |                       1. IE (cours 1)                        |
| 18/09 - 23/09 | - 1. [Architecture matérielle et logicielle d'un ordinateur](../cours/1_archi.md)<br />- 2. [Programmation fonctionnelle avec OCaml](../cours/2_ocaml_fonctionnel.md) |     2. [Système de fichiers](../td/systeme_fichiers.md)      | 2. [Maîtrise du terminal](../tp/terminal.md) : Si vous en avez une, pensez à ramener une clé USB. | 1. [Bases fonctionnelles d'OCaml](../tp/ocaml/bases_fonctionnelles.md) |                         2. IE (TD 1)                         |
| 25/09 - 30/09 |                              -                               |                              -                               |                              -                               |                              -                               |                              -                               |
| 02/10 - 07/10 | 2. [Programmation fonctionnelle avec OCaml](../cours/2_ocaml_fonctionnel.md) |                           -> Cours                           |              4. [Bases du C](../tp/c/bases.md)               | - [Introduction à git](../tp/git.md) : créer un compte `framagit` au préalable (attention il peut y avoir un délai d'une semaine)<br />- 3. [Compilation en C](../tp/c/compilation.md) |                 3. IE (TP 2, TD 2, cours 1)                  |
| 09/10 - 14/10 | - 2. [Programmation fonctionnelle avec OCaml](../cours/2_ocaml_fonctionnel.md)<br />- 3. [Récursivité](../cours/3_recursivite.md) |           3. [Bases d'OCaml](../td/bases_ocaml.md)           | 6. [Déclaration de types](../tp/ocaml/declaration_types.md)  |           5. [Listes OCaml](../tp/ocaml/listes.md)           | - 4. IE (cours 2 et TP 1)<br />- 1. [DS](../evaluations/ds/correction.md) (cours 1 à 3, TD 1 à 3, TP 1 à 4) |
| 16/10 - 21/10 |         3. [Récursivité](../cours/3_recursivite.md)          |            4. [Récursivité](../td/recursivite.md)            | 6. [Déclaration de types](../tp/ocaml/declaration_types.md)  | 6. [Déclaration de types](../tp/ocaml/declaration_types.md)  |                      5. IE (TP 3 et 4)                       |
|   Vacances    |                              -                               |                              -                               |                              -                               |                              -                               | 1. [DM](../evaluations/dm/correction.md) (à déposer sur git pour le 06/11) |
| 06/11 - 10/11 |   4. [Analyse d'algorithmes](../cours/4_analyse_algos.md)    |                           -> Cours                           | 8. [Programmation impérative en OCaml](../tp/ocaml/imperatif.md) | 7. [Pointeurs et gestion de la mémoire en C](../tp/c/pointeurs_gestion_memoire.md) |                 6. IE (TP 5, TD 4, cours 3)                  |
| 13/11 - 18/11 |   4. [Analyse d'algorithmes](../cours/4_analyse_algos.md)    |                           -> Cours                           | 8. [Programmation impérative en OCaml](../tp/ocaml/imperatif.md) | 9. [Tableaux statiques et dynamiques en C](../tp/c/tableaux_statiques_dynamiques.md) |                  7. IE (TP 6, cours 2 et 3)                  |
| 20/11 - 25/11 |   4. [Analyse d'algorithmes](../cours/4_analyse_algos.md)    | 5. [Terminaison et correction](../td/terminaison_correction.pdf) |     11. [Chaînes de caractères en C](../tp/c/chaines.md)     | 10. [Tableaux multidimensionnels en OCaml](../tp/ocaml/tableaux_multidimensionnels.md) |                      8. IE (TP 7, TP 9)                      |
| 27/11 - 02/12 |   4. [Analyse d'algorithmes](../cours/4_analyse_algos.md)    | 5. [Terminaison et correction](../td/terminaison_correction.pdf) |     11. [Chaînes de caractères en C](../tp/c/chaines.md)     |      12. [Quelques tris et leur analyse](../tp/tris.md)      |                     9. IE (TP 8, TP 10)                      |
| 04/12 - 09/12 | - 4. [Analyse d'algorithmes](../cours/4_analyse_algos.md)<br />- 5. [Programmation impérative](../cours/5_imperatif.md) |                           -> Cours                           | 13. [Ligne de commande et structures simples en C](../tp/c/argv_structures.md) |      12. [Quelques tris et leur analyse](../tp/tris.md)      | 2. [DS](../evaluations/ds/correction.md) (cours 1 à 4, TD jusqu'au 5, TP jusqu'au 12) |
| 11/12 - 16/12 |    5. [Programmation impérative](../cours/5_imperatif.md)    |            6. [Complexité](../td/complexite.pdf)             | 13. [Ligne de commande et structures simples en C](../tp/c/argv_structures.md) |       [Colle 1](../evaluations/colles/1_programme.md)        |                10. IE (TP 12, TD 5, cours 4)                 |
| 18/12 - 23/12 |   - 5. [Programmation impérative](../cours/5_imperatif.md)   |            6. [Complexité](../td/complexite.pdf)             | 14. [Structures récursives en C](../tp/c/structures_recursives.md) |       [Colle 1](../evaluations/colles/1_programme.md)        |                    11. IE (TP 11, TP 13)                     |
|   Vacances    |                              -                               |                              -                               |                              -                               |                              -                               | 2. [DM](../evaluations/dm/correction.md) (à déposer sur git pour le 08/01) |
| 08/01 - 13/01 | 6. [Structures séquentielles](../cours/6_structures_sequentielles.md) |                           -> Cours                           | 14. [Structures récursives en C](../tp/c/structures_recursives.md) |       [Colle 1](../evaluations/colles/1_programme.md)        |                              -                               |
| 15/01 - 20/01 | 6. [Structures séquentielles](../cours/6_structures_sequentielles.md) |    7. [Paradigmes de programmation](../td/paradigmes.md)     |                              -                               |                              -                               |                              -                               |
| 22/01 - 27/01 | - 6. [Structures séquentielles](../cours/6_structures_sequentielles.md)<br />- 7. [Validation de programmes](../cours/7_validation.md) |                           -> Cours                           | 15. [Manipulations de structures séquentielles en OCaml](../tp/ocaml/manipulations_structures_sequentielles.md) |       [Colle 1](../evaluations/colles/1_programme.md)        |                        12. IE (TP 14)                        |
| 29/01 - 03/02 |   7. [Validation de programmes](../cours/7_validation.md)    |        8. [Listes chaînées](../td/listes_chainees.md)        |      17. [Piles et files en C](../tp/c/piles_files.md)       | 16. [Interface et implémentation de piles et de files en OCaml](../tp/ocaml/interface_implementation_piles_files.md) | 3. [DS](../evaluations/ds/correction.md) (cours 1 à 6, TD jusqu'au 7, TP jusqu'au 15) |



---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Source des images : *production personnelle*
