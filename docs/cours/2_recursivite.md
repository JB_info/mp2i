# Chapitre 2 : Récursivité

## I. Fonctions récursives

Définition d'un objet récursif et exemples courants.

Exemple de fonction récursive : factorielle.

Définition d'une fonction récursive : cas de base et cas général.

## II. Pile d'appels

Fonctionnement de la pile d'appels (call stack).

Exemples d'évolution de la pile avec fonctions simples.

Pile d'appels dans le cas des fonctions récursives et exemple.

Dépassement de capacité de la pile (stack overflow).

## III. Récursivités particulières

### 1. Récursivité croisée (mutuelle)

Exemple simple (parité d'un entier).

Définition finale d'une fonction récursive.

### 2. Récursivité terminale

Principe et avantage en terme de pile d'appels.

Utilisation d'un accumulateur pour obtenir une fonction récursive terminale.

### 3. Appels récursifs multiples

Exemple simple : la suite de Fibonacci.

Arbre d'appels récursifs.

Complexité spatiale d'une fonction récursive.

## IV. Une structure de données récursive : la liste chaînée

Définition d'une liste chaînée, exemples.

Fonctions récursives pour parcourir une liste.


---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)
