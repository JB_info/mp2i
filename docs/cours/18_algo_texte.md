# Chapitre 18 : Algorithmique du texte

Définitions et syntaxe : alphabet, mot, taille, concaténation, préfixe, suffixe, facteur, occurrence.

Nombre de préfixes, de suffixes et de facteur d'un mot.

L'algorithmique du texte fait référence à une famille d'algorithme manipulant des mots, pour lesquels le nombre de caractères de l'alphabet est négligeable devant la taille des mots.

## I. Recherche textuelle

Spécifications du problème.

Recherche textuelle par force brute : principe et complexité.

### 1. Algorithme de Rabin-Karp

Principe de l'algorithme.

Déroulé de l'algorithme sur un exemple.

Critères pour le choix de la fonction de hachage.

Complexité de l'algorithme.

### 2. Algorithme de Boyer-Moore

Principe de la version de Horspool (une seule table de décalage).

Déroulé de l'algorithme Boyer-Moore-Horspool sur un exemple : prétraitement puis recherche.

L'algorithme complet de Boyer-Moore utilise plusieurs fonctions de décalage : exemple avec la règle du bon suffixe.

Complexité de l'algorithme.

## II. Compression

Spécifications du problème.

### 1. Algorithme de Huffman

Principe de l'algorithme de compression.

Déroulé de l'algorithme sur un exemple.

Principe de la décompression associée.

Propriétés :

* Le code construit est préfixe, la compression est donc sans perte.
* L'algorithme de Huffman est optimal pour un code préfixe.

### 2. Algorithme de Lempel-Ziv-Welch

Principe de l'algorithme de compression.

Déroulé de l'algorithme sur un exemple.

Principe de la décompression associée.


---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Source des images : *production personnelle*
