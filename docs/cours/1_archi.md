# Chapitre 1 : Architecture matérielle et logicielle d'un ordinateur

## I. Généralités

### 1. Qu'est-ce qu'un « ordinateur » ?

Origines de nos ordinateurs actuels.

Notion de machine universelle.

Définition d'un ordinateur.

### 2. Qu'est-ce qu'un « algorithme » ?

Introduction à la notion de décidabilité et de calculabilité.

Définition d'un algorithme.

## II. Architecture d'un ordinateur

### 1. Architecture de Von Neumann

Principe de l'architecture.

Schéma exposant les composants de l'architecture de Von Neumann :

![](img/1_archi/von_neumann.png)

Rôle des principaux composants.

### 2. Organisation de la mémoire

Caractéristiques de la mémoire vive, de la mémoire morte et de la mémoire de masse.

### 3.  Exécution d'un programme

Fonctionnement des registres et rôle de l'unité de contrôle dans le cycle d'exécution d'un programme.

## III. Système d'exploitation

Exemples de systèmes d'exploitation.

Principales mission de l'OS.

## IV. Système de fichiers

### 1. Point de vue logique

#### a. Arborescence de fichiers

Exemple d'arborescence de fichiers.

Répertoire racine et répertoires principaux des systèmes de fichiers sous Linux.

#### b. Manipulation des fichiers

Chemin absolu et chemin relatif.

Introduction à la ligne de commande (terminal, shell), et commandes principales du terminal pour la manipulation des fichiers.

#### c. Droits fondamentaux sur les fichiers

Droits de lecture, d'écriture et d'exécution.

Commande `chmod`.

### 2. Point de vue physique

Tous les objets de l'arborescence sont des fichiers.

Implémentation interne : blocs et nœuds d’index (inode).

Liens physiques et symboliques.

![](img/1_archi/indirections.png)

## V. Programmation

Définition d'un programme, d'un processus, d'un langage de programmation, d'un paradigme de programmation.

Redirections dans l’interface système et tubes (pipe).

Langages compilés, langages interprétés.

![](img/1_archi/compilateur.png)

![](img/1_archi/interpreteur.png)


---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Source des images : *production personnelle*



