# Chapitre 17 : Plus court chemin dans un graphe pondéré

## I. Notion de plus court chemin

Rappels : définition d'un graphe pondéré, du poids d'un chemin dans un graphe pondéré.

Matrice d'adjacence d'un graphe pondéré.

Distance et plus court chemin entre deux sommets dans un graphe pondéré.

Spécification du problème d'optimisation.

Optimalité des sous-chemins d'un plus court chemin.

## II. Algorithme de Floyd-Warshall

Principe de l'algorithme, relation de récurrence.

Algorithme en pseudo-code, analyse.

Application à la main de l'algorithme, exemple.

## III. Algorithme de Dijkstra

Principe de l'algorithme, preuve d'optimalité.

Algorithme en pseudo-code, analyse.

Application à la main de l'algorithme, exemple.

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Source des images : *production personnelle*, A. Lick
