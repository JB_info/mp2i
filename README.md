# Informatique - MP2I

* [Calendrier du semestre 1](docs/calendrier/s1.md)
* [Calendrier du semestre 2](docs/calendrier/s2.md)

![logo](logo.png)

## Infos générales

* Me contacter : [mon adresse mail](mailto:benouwt.info@gmail.com)
* Guide de rentrée : [ici](docs/guide_rentree.md)
* Résumé des éléments de syntaxe au programme pour les langages C et OCaml : [ici](docs/cours/c_ocaml_programme.pdf)
* Lexique des abréviations pour les devoirs : [ici](docs/evaluations/abreviations.md)
* Le programme : [première & deuxième année](https://prepas.org/index.php?document=73)
* IDE en ligne : [OCaml](https://try.ocamlpro.com/) - [C](https://www.onlinegdb.com/online_c_compiler) - [SQL](https://sqliteonline.com/) - [Autres](https://replit.com/templates)
* Concours : [SCEI](https://www.scei-concours.fr/) - [Mines-Ponts](https://www.concoursminesponts.fr/) - [ENS](https://www.ens.psl.eu/une-formation-d-exception/admission-concours/concours-voie-cpge/concours-voie-cpge-sciences) - [CCINP](https://www.concours-commun-inp.fr/fr/index.html) - [Centrale](https://www.concours-centrale-supelec.fr/)
* Ressources supplémentaires : [ici](docs/ressources.md)

---

Par *Justine BENOUWT*

Sous licence [*CC BY-NC-SA*](https://creativecommons.org/licenses/by-nc-sa/4.0/)

![CC BY-NC-SA](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

Source des images : *production personnelle*
